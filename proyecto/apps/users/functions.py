from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
import json

def LogIn(request, username, password):
	dic = {}
	user = authenticate(username = username, password = password)
	if user is not None:
		if user.is_active:
			login(request, user)
			dic = {'msg' : 'ok'}
		else:
			dic = {'msg' : 'noActivo'}
	else:
		dic = {'msg' : 'noExiste'}
	return dic