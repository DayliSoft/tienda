
from django.conf.urls import url, include
from .models import User
from rest_framework import routers, serializers, viewsets
from django.contrib.auth.decorators import login_required

# @login_required(login_url='/api-auth/login/')
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
