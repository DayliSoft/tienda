# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import *
from django.contrib.auth.forms import UserChangeForm
from django.forms import ModelForm
from suit.widgets import SuitDateWidget, SuitTimeWidget, SuitSplitDateTimeWidget
# Register your models here.

# @admin.register(User)
class UserForm(ModelForm):
    class Meta:
        model = User
        fields = '__all__'
        widgets = {
            'last_login': SuitSplitDateTimeWidget,
        }

class UserAdmin(admin.ModelAdmin):

    form = UserForm
    search_fields = ('username', 'us_nombre')
    list_display = ('username', 'email', 'us_nombre', 'us_apellidos','is_superuser')
    list_filter = ('is_superuser','is_active',)
    list_select_related = True
    filter_horizontal = ('groups', 'user_permissions')

    fieldsets = (
            (None, {'fields' : ('username', 'password')}),
            ('Información Personal', {'fields' : (
                            'us_nombre',
                            'us_apellidos',
                            'email',
                            'us_telefono_movil',
                            'us_avatar'
                    )}),
            ('Permisos', {'fields': (
                            'is_active',
                            'is_staff',
                            'is_superuser',
                            'groups',
                            'user_permissions',
                )}),
            (None, {'fields': (
                            'last_login',     
                )}),
        )

admin.site.register(User,UserAdmin)

class PerfilAdmin(admin.ModelAdmin):

    list_display = ('user', 'activation_key', 'key_expires')

admin.site.register(UserProfile, PerfilAdmin)