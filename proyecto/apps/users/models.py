# -*- coding: utf-8 -*-
from django.db import models
import datetime
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.timezone import now
from django.db import connection
import cx_Oracle

# Create your models here.

class UserManager(BaseUserManager, models.Manager):

    def _create_user(self, username, email, us_nombre,us_apellidos,
                us_telefono_movil, password, is_staff,is_superuser,is_active, **extra_fields):

        email = self.normalize_email(email)
        if not email:
            raise ValueError('El email debe ser obligatorio')
        user = self.model(username = username, email=email, us_nombre=us_nombre,
                        us_apellidos=us_apellidos, us_telefono_movil=us_telefono_movil, is_active=is_active, is_staff = is_staff, is_superuser = is_superuser, **extra_fields)
        user.set_password(password)
        user.save( using = self._db)
        return user

    def create_user(self, username, email, us_nombre, us_apellidos, us_telefono_movil, password=None, **extra_fields):
        return self._create_user(username, email, us_nombre, us_apellidos, us_telefono_movil, password, False, False,False, **extra_fields)

    def create_superuser(self, username, email, password=None, **extra_fields):
        return self._create_user(username, email,'none','none','none',password, True, True,True, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):

    username = models.CharField(max_length=100, unique=True,verbose_name="Username",
                                help_text='Debe ser único, sin espacios ni caracteres especiales.')
    us_nombre = models.CharField(max_length=100,verbose_name="Nombres",
                                help_text='Nombres del usuario.')
    us_apellidos = models.CharField(max_length=100,verbose_name="Apellidos",
                                    help_text='Apellidos del usuario.')
    us_telefono_movil = models.CharField(max_length=10,verbose_name="Telefono",
                                        help_text='Telefono o movil del usuario.')
    us_avatar = models.ImageField(upload_to = 'users', null=True,blank=True,verbose_name="Avatar",default='/static/daylisoft/img/sin-imagen-user.png')
    email = models.EmailField(unique=True,verbose_name="Email",
                                help_text='Cuenta de mail del usuario, debe ser unica.')

    objects = UserManager()

    is_active = models.BooleanField(default = True,
                                    help_text='Designa si el usuario debe ser tratado como activo.')
    is_staff = models.BooleanField(default = False,
                                    help_text='Indica si el usuario puede iniciar sesión en el sitio de administracíon.')

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def new_user(self, username, us_password, us_nombre, us_apellidos , us_telefono_movil ,email, us_avatar) :
        cursor = connection.cursor()
        ret = cursor.callproc("new_user", (username,  us_password, us_nombre ,us_apellidos , us_telefono_movil , email, us_avatar))
        cursor.close()
        return ret
    def update_user(self, us_id, username, us_nombre, us_apellidos , us_telefono_movil ,email, us_avatar):
        cursor = connection.cursor()
        ret = cursor.callproc("update_user", (us_id, username, us_nombre, us_apellidos , us_telefono_movil ,email, us_avatar))
        cursor.close()
        return ret
    def update_user_inactivo(self, user_id, is_active):
        cursor = connection.cursor()
        ret = cursor.callproc("update_user_inactivo", (user_id, is_active))
        cursor.close()
        return ret
    def update_user_superuser(self, user_id, is_superuser):
        cursor = connection.cursor()
        ret = cursor.callproc("update_user_superuser", (user_id, is_superuser))
        cursor.close()
        return ret
    def update_user_password(self, user_id, password):
        cursor = connection.cursor()
        ret = cursor.callproc("update_user_password", (user_id, password))
        cursor.close()
        return ret
    def view_user(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_user",cx_Oracle.CURSOR)
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'id':i[0],'password':i[1], 'is_superuser': i[3],
                                'username': i[4],  'us_nombre': i[5], 'us_apellidos': i[6],
                                'us_telefono_movil': i[7], 'us_avatar': i[8],
                                'email': i[9], 'is_active': i[10],
                                'is_staff': i[11]})
        return datosQuery
    def view_user_id(self, user_id):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_user_id",cx_Oracle.CURSOR, (user_id, 0))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'id':i[0], 'password':1, 'is_superuser': i[3],
                                'username': i[4],  'us_nombre': i[5], 'us_apellidos': i[6],
                                'us_telefono_movil': i[7], 'us_avatar': i[8],
                                'email': i[9], 'is_active': i[10],
                                'is_staff': i[11]})
        return datosQuery
    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return self.us_nombre+' '+self.us_apellidos

    class Meta:
        verbose_name_plural=u'Usuarios'


class UserProfile(models.Model):
    user = models.ForeignKey(User)
    activation_key = models.CharField(max_length=40, blank=True)
    key_expires = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural=u'Perfiles de Usuario'