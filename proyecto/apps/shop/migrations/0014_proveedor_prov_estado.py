# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0013_auto_20161204_1244'),
    ]

    operations = [
        migrations.AddField(
            model_name='proveedor',
            name='prov_estado',
            field=models.BooleanField(default=True),
        ),
    ]
