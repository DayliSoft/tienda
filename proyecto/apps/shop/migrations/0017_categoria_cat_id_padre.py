# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0016_remove_categoria_cat_id_padre'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoria',
            name='cat_id_padre',
            field=models.ForeignKey(blank=True, to='shop.categoria', null=True),
        ),
    ]
