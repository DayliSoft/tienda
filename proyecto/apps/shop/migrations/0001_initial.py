# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='categoria',
            fields=[
                ('cat_id', models.AutoField(serialize=False, primary_key=True)),
                ('cat_nombre', models.CharField(max_length=30)),
                ('cat_descripcion', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='det_pedido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('precio_unit', models.DecimalField(max_digits=10, decimal_places=2)),
                ('cantidad', models.IntegerField()),
                ('descuento', models.DecimalField(max_digits=2, decimal_places=2)),
                ('total_pagar', models.DecimalField(max_digits=10, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='emp_trasporte',
            fields=[
                ('trans_id', models.AutoField(serialize=False, primary_key=True)),
                ('trans_nombre', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='imagen_producto',
            fields=[
                ('img_id', models.AutoField(serialize=False, primary_key=True)),
                ('img_data', models.ImageField(upload_to=b'imagenes_productos')),
            ],
        ),
        migrations.CreateModel(
            name='marca',
            fields=[
                ('mar_id', models.AutoField(serialize=False, primary_key=True)),
                ('mar_nombre', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='oferta',
            fields=[
                ('ofer_id', models.AutoField(serialize=False, primary_key=True)),
                ('ofer_descripcion', models.TextField()),
                ('ofer_fecha_inicio', models.DateTimeField()),
                ('ofer_fecha_fin', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='pedido',
            fields=[
                ('ped_id', models.AutoField(serialize=False, primary_key=True)),
                ('ped_fecha_pedido', models.DateTimeField(auto_now_add=True)),
                ('ped_fecha_envio', models.DateTimeField(null=True, blank=True)),
                ('ped_direccion_envio', models.TextField()),
                ('ped_ciudad_envio', models.CharField(max_length=50)),
                ('ped_cod_postal_envio', models.CharField(max_length=10)),
                ('ped_pais_envio', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='producto',
            fields=[
                ('pro_id', models.AutoField(serialize=False, primary_key=True)),
                ('pro_nombre', models.CharField(max_length=50)),
                ('pro_precio', models.DecimalField(max_digits=10, decimal_places=2)),
                ('pro_descripcion_corta', models.CharField(max_length=50)),
                ('pro_descripcion_larga', models.TextField()),
                ('pro_destacado', models.BooleanField(default=False)),
                ('pro_estado', models.BooleanField(default=True)),
                ('pro_stock', models.IntegerField()),
                ('cat_id', models.ForeignKey(to='shop.categoria')),
                ('mar_id', models.ForeignKey(to='shop.marca')),
            ],
        ),
        migrations.CreateModel(
            name='proveedor',
            fields=[
                ('prov_id', models.AutoField(serialize=False, primary_key=True)),
                ('prov_nombre', models.CharField(max_length=50)),
                ('prov_contacto', models.CharField(max_length=50)),
                ('prov_direccion', models.CharField(max_length=50)),
                ('prov_ciudad', models.CharField(max_length=20)),
                ('prov_cod_postal', models.CharField(max_length=10)),
                ('prov_pais', models.CharField(max_length=10)),
                ('prov_telefono', models.CharField(max_length=10)),
                ('prov_cel', models.CharField(max_length=10)),
                ('prov_home_page', models.CharField(max_length=30, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='tag',
            fields=[
                ('tag_id', models.AutoField(serialize=False, primary_key=True)),
                ('tag_nombre', models.CharField(max_length=8)),
            ],
        ),
        migrations.CreateModel(
            name='tipo_oferta',
            fields=[
                ('tpof_id', models.AutoField(serialize=False, primary_key=True)),
                ('tpof_nombre', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='producto',
            name='tags',
            field=models.ManyToManyField(related_name='pro_tags', to='shop.tag'),
        ),
    ]
