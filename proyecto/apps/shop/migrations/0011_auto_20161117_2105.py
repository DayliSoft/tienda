# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0010_remove_categoria_slug'),
    ]

    operations = [
        migrations.RenameField(
            model_name='categoria',
            old_name='parent',
            new_name='cat_padre_id',
        ),
    ]
