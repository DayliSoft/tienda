# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0014_proveedor_prov_estado'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='oferta',
            name='pro_id',
        ),
        migrations.RemoveField(
            model_name='producto_cuarentena',
            name='producto',
        ),
        migrations.RemoveField(
            model_name='review',
            name='rev_producto',
        ),
        migrations.RemoveField(
            model_name='review',
            name='rev_usuario',
        ),
        migrations.DeleteModel(
            name='oferta',
        ),
        migrations.DeleteModel(
            name='producto_cuarentena',
        ),
        migrations.DeleteModel(
            name='review',
        ),
    ]
