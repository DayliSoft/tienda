# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0019_remove_categoria_cat_id_padre'),
    ]

    operations = [
        migrations.DeleteModel(
            name='proveedor',
        ),
        migrations.RemoveField(
            model_name='categoria',
            name='level',
        ),
        migrations.RemoveField(
            model_name='categoria',
            name='lft',
        ),
        migrations.RemoveField(
            model_name='categoria',
            name='rght',
        ),
        migrations.RemoveField(
            model_name='categoria',
            name='tree_id',
        ),
    ]
