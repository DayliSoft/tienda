# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0020_auto_20170122_2136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedido',
            name='ped_direccion_envio',
            field=models.CharField(max_length=300),
        ),
    ]
