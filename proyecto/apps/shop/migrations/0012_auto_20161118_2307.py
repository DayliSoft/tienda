# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0011_auto_20161117_2105'),
    ]

    operations = [
        migrations.RenameField(
            model_name='categoria',
            old_name='cat_padre_id',
            new_name='cat_id_padre',
        ),
    ]
