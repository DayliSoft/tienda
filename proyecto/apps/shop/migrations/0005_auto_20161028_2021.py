# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20160928_1335'),
    ]

    operations = [
        migrations.AddField(
            model_name='oferta',
            name='ofer_porcentaje',
            field=models.DecimalField(default=1, max_digits=10, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='oferta',
            name='ofer_fecha_fin',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='oferta',
            name='ofer_fecha_inicio',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='tag',
            name='tag_nombre',
            field=models.CharField(max_length=20),
        ),
    ]
