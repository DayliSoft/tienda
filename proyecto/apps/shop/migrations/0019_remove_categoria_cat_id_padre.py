# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0018_auto_20170122_2055'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='categoria',
            name='cat_id_padre',
        ),
    ]
