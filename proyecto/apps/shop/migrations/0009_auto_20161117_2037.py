# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0008_auto_20161117_2034'),
    ]

    operations = [
        migrations.RenameField(
            model_name='categoria',
            old_name='cat_id_padre',
            new_name='parent',
        ),
        migrations.AddField(
            model_name='categoria',
            name='slug',
            field=models.SlugField(default=1, editable=False),
            preserve_default=False,
        ),
    ]
