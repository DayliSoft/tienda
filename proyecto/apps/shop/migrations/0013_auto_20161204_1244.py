# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0012_auto_20161118_2307'),
    ]

    operations = [
        migrations.AddField(
            model_name='emp_trasporte',
            name='trans_costo',
            field=models.DecimalField(default=1, max_digits=10, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='emp_trasporte',
            name='trans_url',
            field=models.CharField(default=1, max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='pedido',
            name='ped_cod_deposito',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='pedido',
            name='ped_codigo_guia',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='pedido',
            name='ped_estado',
            field=models.CharField(default=b'pendiente', max_length=20),
        ),
        migrations.AddField(
            model_name='pedido',
            name='ped_imagen_deposito',
            field=models.ImageField(default=b'/static/daylisoft/img/sin_imagen.gif', null=True, upload_to=b'depositos', blank=True),
        ),
        migrations.AddField(
            model_name='pedido',
            name='ped_total_pagar',
            field=models.DecimalField(default=1, max_digits=10, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='producto',
            name='pro_precio_compra',
            field=models.DecimalField(default=1, max_digits=10, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='det_pedido',
            name='descuento',
            field=models.DecimalField(max_digits=10, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='pedido',
            name='trans_id',
            field=models.ForeignKey(blank=True, to='shop.emp_trasporte', null=True),
        ),
    ]
