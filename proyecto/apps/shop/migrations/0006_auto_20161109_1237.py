# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_auto_20161028_2021'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='pro_destacado',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='producto',
            name='pro_valoracion',
            field=models.DecimalField(default=1, max_digits=10, decimal_places=2),
            preserve_default=False,
        ),
    ]
