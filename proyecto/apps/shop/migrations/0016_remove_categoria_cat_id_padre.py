# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0015_auto_20170121_1606'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='categoria',
            name='cat_id_padre',
        ),
    ]
