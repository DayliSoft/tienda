# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0003_auto_20160926_1650'),
    ]

    operations = [
        migrations.CreateModel(
            name='producto_cuarentena',
            fields=[
                ('pro_c_id', models.AutoField(serialize=False, primary_key=True)),
                ('pro_c_cantidad', models.IntegerField()),
                ('pro_c_nota', models.TextField()),
            ],
        ),
        migrations.RemoveField(
            model_name='oferta',
            name='ofer_descripcion',
        ),
        migrations.RemoveField(
            model_name='oferta',
            name='tpof_id',
        ),
        migrations.RemoveField(
            model_name='producto',
            name='pro_destacado',
        ),
        migrations.RemoveField(
            model_name='producto',
            name='pro_precio_otros',
        ),
        migrations.RemoveField(
            model_name='proveedor',
            name='prov_cod_postal',
        ),
        migrations.AddField(
            model_name='producto',
            name='pro_stock_minimo',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='review',
            name='rev_fecha',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.RemoveField(
            model_name='producto',
            name='categorias',
        ),
        migrations.AddField(
            model_name='producto',
            name='categorias',
            field=models.ForeignKey(default=1, to='shop.categoria'),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='tipo_oferta',
        ),
        migrations.AddField(
            model_name='producto_cuarentena',
            name='producto',
            field=models.ForeignKey(to='shop.producto'),
        ),
    ]
