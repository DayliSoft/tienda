# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0017_categoria_cat_id_padre'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categoria',
            name='cat_descripcion',
            field=models.CharField(max_length=300),
        ),
    ]
