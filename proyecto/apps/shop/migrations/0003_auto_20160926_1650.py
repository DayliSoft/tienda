# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shop', '0002_auto_20160904_1117'),
    ]

    operations = [
        migrations.CreateModel(
            name='review',
            fields=[
                ('rev_id', models.AutoField(serialize=False, primary_key=True)),
                ('rev_texto', models.TextField()),
                ('rev_estrellas', models.IntegerField()),
            ],
            options={
                'verbose_name_plural': 'Reviews',
            },
        ),
        migrations.AlterModelOptions(
            name='categoria',
            options={'verbose_name_plural': 'Categorias'},
        ),
        migrations.AlterModelOptions(
            name='det_pedido',
            options={'verbose_name_plural': 'Detalle pedidos'},
        ),
        migrations.AlterModelOptions(
            name='emp_trasporte',
            options={'verbose_name_plural': 'Transportes'},
        ),
        migrations.AlterModelOptions(
            name='imagen_producto',
            options={'verbose_name_plural': 'Imagenes productos'},
        ),
        migrations.AlterModelOptions(
            name='marca',
            options={'verbose_name_plural': 'Marcas'},
        ),
        migrations.AlterModelOptions(
            name='oferta',
            options={'verbose_name_plural': 'Ofertas'},
        ),
        migrations.AlterModelOptions(
            name='pedido',
            options={'verbose_name_plural': 'Pedidos'},
        ),
        migrations.AlterModelOptions(
            name='producto',
            options={'verbose_name_plural': 'Productos'},
        ),
        migrations.AlterModelOptions(
            name='proveedor',
            options={'verbose_name_plural': 'Proveedores'},
        ),
        migrations.AlterModelOptions(
            name='tag',
            options={'verbose_name_plural': 'Tags'},
        ),
        migrations.AlterModelOptions(
            name='tipo_oferta',
            options={'verbose_name_plural': 'Tipos de Ofertas'},
        ),
        migrations.RemoveField(
            model_name='producto',
            name='cat_id',
        ),
        migrations.AddField(
            model_name='imagen_producto',
            name='img_es_principal',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='producto',
            name='categorias',
            field=models.ManyToManyField(related_name='pro_categorias', to='shop.categoria'),
        ),
        migrations.AddField(
            model_name='producto',
            name='pro_info_adicional',
            field=models.CharField(default=1, max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='producto',
            name='pro_precio_otros',
            field=models.DecimalField(default=1, max_digits=10, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='categoria',
            name='cat_id_padre',
            field=models.ForeignKey(blank=True, to='shop.categoria', null=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='pro_descripcion_corta',
            field=models.CharField(max_length=360),
        ),
        migrations.AddField(
            model_name='review',
            name='rev_producto',
            field=models.ForeignKey(to='shop.producto'),
        ),
        migrations.AddField(
            model_name='review',
            name='rev_usuario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
