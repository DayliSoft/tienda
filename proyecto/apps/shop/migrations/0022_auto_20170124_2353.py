# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0021_auto_20170124_1416'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedido',
            name='ped_fecha_envio',
            field=models.CharField(max_length=150, null=True, blank=True),
        ),
    ]
