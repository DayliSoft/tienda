# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='pedido',
            name='cliente',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='pedido',
            name='trans_id',
            field=models.ForeignKey(to='shop.emp_trasporte'),
        ),
        migrations.AddField(
            model_name='oferta',
            name='pro_id',
            field=models.ForeignKey(to='shop.producto'),
        ),
        migrations.AddField(
            model_name='oferta',
            name='tpof_id',
            field=models.ForeignKey(to='shop.tipo_oferta'),
        ),
        migrations.AddField(
            model_name='imagen_producto',
            name='pro_id',
            field=models.ForeignKey(to='shop.producto'),
        ),
        migrations.AddField(
            model_name='det_pedido',
            name='ped_id',
            field=models.ForeignKey(to='shop.pedido'),
        ),
        migrations.AddField(
            model_name='det_pedido',
            name='pro_id',
            field=models.ForeignKey(to='shop.producto'),
        ),
        migrations.AddField(
            model_name='categoria',
            name='cat_id_padre',
            field=models.ForeignKey(to='shop.categoria', null=True),
        ),
    ]
