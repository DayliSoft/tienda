# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0006_auto_20161109_1237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categoria',
            name='cat_id_padre',
            field=mptt.fields.TreeForeignKey(related_name='children', blank=True, to='shop.categoria', null=True),
        ),
    ]
