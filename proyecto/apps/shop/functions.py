# -*- coding: utf-8 -*-
from django.core.paginator import Paginator , EmptyPage, PageNotAnInteger
from .models import *
from datetime import time, date
from apps.proyecto_admin.models import perfil_empresa
from apps.proyecto_admin.models import impuesto
from django.template.loader import render_to_string
from django.core.mail import send_mail
from decimal import *
from twilio.rest import TwilioRestClient
from twilio import TwilioRestException

'''PARAMETROS:
request: Request de la vista
modelo: Modelo a utilizar en la paginación
paginas: Cantidad de paginas del paginador
nonRepPag: Identificador del result list
'''
def Paginador(request, modelo, paginas, nonRegPag):

    #Retorna el objeto paginator para comenzar el trabajo
    result_list = Paginator(modelo, paginas)
    try:
     #Tomamos el valor de parametro page, usando GET
     page = int(request.GET.get('page'));
    except:
     page = 1

    #Si es menor o igual a 0 igualo en 1
    if page <= 0:
        page = 1

    #Si el parámetro es mayor a la cantidad
    #de paginas le igualo el parámetro con las cant de paginas
    if(page > result_list.num_pages):
        page = result_list.num_pages

    #Verificamos si esta dentro del rango
    if (result_list.num_pages >= page):
        #Obtengo el listado correspondiente al page
        pagina = result_list.page(page)
        Contexto = {nonRegPag: pagina.object_list, #Asignamos registros de la pagina
                   'page': page, #Pagina Actual
                   'pages': result_list.num_pages, #Cantidad de Paginas
                   'has_next': pagina.has_next(), #True si hay proxima pagina
                   'has_prev': pagina.has_previous(), #true si hay Pagina anterior
                   'next_page': page+1, #Proxima pagina
                   'prev_page': page-1, #Pagina Anterior
                   'firstPage': 1,
                   }
        return Contexto

# Valida si la cantidad del producto existen en la bd
def validar_producto(find_prod,cantidad, nombre):
    print(find_prod, cantidad)
    if int(cantidad) <= int(find_prod):
        dic = {
            "result" : "OK"
        }
    else:
        dic = {
            "result" : "PRODUCTO",
            "mensaje" : "El producto "+nombre+" no cuenta con la cantidad de "+str(cantidad)+" unidades disponibles por el momento."
        }
    return dic

