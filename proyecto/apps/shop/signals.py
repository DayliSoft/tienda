from django.db.models.signals import post_save, pre_delete
from apps.shop.models import det_pedido, producto, pedido, imagen_producto
from django.core.mail import send_mail
from django.template.loader import render_to_string

def actualizar_cant_prod(sender, instance, **kwards):
    print 'actualizando cantidades de productos'
    try:
        find_prod = producto.objects.get(pro_id=instance.pro_id.pk)
        nueva_cant = int(find_prod.pro_stock) - int(instance.cantidad)
        find_prod.pro_stock = nueva_cant
        find_prod.save()
    except Exception as e:
        print e

def eliminar_pedido(sender, instance, **kwards):
    print 'vamo a restablecer cantidades'
    if instance.ped_id.ped_estado != 'enviado':
        try:
            find_prod = producto.objects.get(pk = instance.pro_id.pk)
            find_prod.pro_stock = int(find_prod.pro_stock) + int(instance.cantidad)
            find_prod.save()
        except Exception as e:
            print 'No existe el producto a restaurar las cantidad'

post_save.connect(actualizar_cant_prod, sender=det_pedido)
pre_delete.connect(eliminar_pedido, sender=det_pedido)