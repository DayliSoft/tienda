# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404, render_to_response, RequestContext
from .models import *
from .functions import *
from datetime import datetime, date, time, timedelta
from apps.users.models import *
from django.http import JsonResponse,HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.db import transaction
import json
from apps.proyecto_admin.models import impuesto
from decimal import *
from django.db.models import Q
# Create your views here.

def productos_generales(request,clave_valor):
    print clave_valor
    productos = []
    datosQuery = []
    clave = clave_valor.split("=")
    txt_search = {
        "id_buscado":clave[1]
    }
    if clave[0]=='q':
        if clave[1]=='all':
            find_productos = producto().view_producto_exclude_estado()
            txt_search["tipo"] = 'all'
    elif clave[0]=='cat':
        datosQuery=categoria().view_categoria_nombre(clave[1])
        print(datosQuery[0][0])
        find_productos=producto().view_producto_by_filter(datosQuery[0][0])
        txt_search["tipo"] = 'cat'
    else:
        find_productos = producto().view_producto_exclude_estado()
        txt_search["tipo"] = 'all'

    #Inicio el paginador
    pag = Paginador(request, find_productos, 12, 'productos')
    time = date.today()
    for find_prod in pag['productos']:
        productos.append({
            'prod' : find_prod,
            'img' : '/media/'+str(find_prod['pro_imagen']),
            'oferta': False,
        })

    print txt_search
    #Contexto a retornar a la vista
    cxt = {
    'productos': productos,
    'totPost': find_productos,
    'paginator': pag,
    'categorias': categoria().view_categoria(),
    'respuesta' : txt_search,
    }
    return render_to_response('shop/shop.html', context_instance=RequestContext(request, cxt))

def redirec_prod(request):
    return redirect('/shop/productos/q=all/')

def producto_detalle(request, id):
    try:
        time = date.today()
        registro=producto()
        datosQuery=[]
        datosQuery1=[]
        find_prod = registro.view_producto_id(id)
        datosQuery1=categoria().view_categoria_id(find_prod[0]['categorias'])
        datosQuery.append({'img_id':1,'img_data':'/media/'+str(find_prod[0]['pro_imagen']), 'pro_id_id':id ,'img_es_principal':1})
        dic = {
                'producto' : find_prod,
                'imagenes' : datosQuery,
                'categoria': datosQuery1[0]['cat_nombre'],
                'oferta': False,
                'categorias': categoria().view_categoria()
            }

    except Exception as e:
        dic = {}
        print e
    return render_to_response('shop/shop_product.html',dic, context_instance=RequestContext(request))

def ver_carrito(request):
    iva=[]
    for i in impuesto().view_impuesto_nombre(0, 'iva'):
        iva.append([i[0],i[1],i[2]])
    dic = {
        'transportes' : emp_trasporte().view_transporte(),
        'iva' : iva[0][2]
    }
    return render_to_response('shop/shop_cart.html',dic,context_instance=RequestContext(request))
import time
@transaction.atomic()
def gestionar_compra(request):
    dic = {}
    datos = []
    if request.is_ajax():
        if request.method == 'POST':
            if request.user.is_authenticated():
                transaccion = transaction.savepoint()
                print request.POST
                try:
                    if request.POST['empresa_trans'] != 'empresa':
                        find_empresa = emp_trasporte().view_transporte_id(request.POST['empresa_trans'])
                        find_empresa=find_empresa[0]['trans_id']
                    else:
                        find_empresa = 0
                    registro=pedido()
                    datos=registro.new_pedido(request.user.id, request.POST['direccion'],
                                            request.POST['ciudad'], request.POST['cod_postal'],
                                            'Ecuador', find_empresa, request.POST['subtotal'])
                    for i in datos:
                        idPedido=i[0]
                    for producto_carrito in request.POST.getlist('carrito[]'):
                        producto_carrito = json.loads(producto_carrito)
                        find_prod=producto().view_producto_id(producto_carrito['ID'])
                        dic = validar_producto(find_prod[0]['pro_stock'], producto_carrito['Cantidad'], find_prod[0]['pro_nombre'])
                        print('3')
                        precio_ofer = Decimal(str(producto_carrito['Precio']).replace(',','.'))
                        print('4')
                        descuento = (Decimal(find_prod[0]['pro_precio']) - precio_ofer) * int(producto_carrito['Cantidad'])
                        print('5')
                        if dic['result'] == 'OK':
                            detallePedido=det_pedido().new_detalle_pedido(find_prod[0]['pro_id'],
                                                        idPedido,find_prod[0]['pro_precio'], producto_carrito['Cantidad'],
                                                        descuento, Decimal(str(producto_carrito['Total']).replace(',','.')))
                        else:
                            raise Exception(dic['result'], dic['mensaje'])


                    transaction.savepoint_commit(transaccion)
                    dic = {'result': 'OK'}
                except Exception as e:
                    print (e, 'error')
                    transaction.savepoint_rollback(transaccion)
            else:
                dic = {'result' : 'LOGEATE'}
        else:
            dic = {'result': 'ERROR'}
    else:
        dic = {'result': 'ERROR'}
    response = JsonResponse(dic)
    return HttpResponse(response.content)

@login_required(login_url='/')
def gestion_pedidos(request):
    datos=[]
    datos=pedido().view_pedido_cliente(request.user.id)
    dic = {
        'pedidos' : datos
    }
    return render_to_response('shop/shop_pedidos.html',dic,context_instance=RequestContext(request))

def ingresar_deposito(request):
    print request.POST
    try:
        pedido().update_deposito(request.POST['txt_id'], request.POST['txt_deposito'], 'verificacion', request.FILES['file1'])
    except Exception as e:
        print 'Erro'
    dic = {
        'result' : 'OK'
    }
    response = JsonResponse(dic)
    return HttpResponse(response.content)

@login_required(login_url='/')
def pedido_detalle(request, id):
    try:
        find_pedido=pedido().view_pedido_id(id)
        find_detalle_pedido=det_pedido().view_detalle_pedido_id(id)
        # find_pedido = pedido.objects.get(ped_id=id, cliente=request.user)
        # find_detalle_pedido = det_pedido.objects.filter(ped_id=find_pedido)
        find_iva = []
        cost = []
        find_iva = impuesto().view_impuesto_nombre(0,'iva')
        for i in find_iva:
            porcentaje=i[2]
        if len(find_detalle_pedido)>0:
            subtotal = 0
            for det_ped in find_detalle_pedido:
                subtotal = subtotal + det_ped['total_pagar']
            if find_pedido[0]['trans_id'] != None:
                cost=emp_trasporte().view_transporte_id(find_pedido[0]['trans_id'])
                gasto_envio = cost[0]['trans_costo']
            else:
                gasto_envio = 0
            dic = {
                "pedidos" : find_detalle_pedido,
                "pedido_id" : id,
                "iva" : porcentaje,
                "precio_iva" : round(subtotal * ((porcentaje)/100), 2),
                "subtotal" : subtotal,
                "gasto_envio" : gasto_envio,
                "total_pagar" : find_pedido[0]['ped_total_pagar']
            }
        else:
            return redirect('/shop/pedidos/')
    except Exception as e:
        print e
        return redirect('/shop/pedidos/')
    return render_to_response('shop/shop_pedido_detalle.html',dic,context_instance=RequestContext(request))
# FALTA APLICAR EL ORACLE

def consulta_transporte(request):
    print request.POST
    try:
        precio=[]
        precio=emp_trasporte().view_transporte_id(request.POST['id_empresa'])
        dic = {
            'result' : 'OK',
            'precio' : precio[0]['trans_costo']
        }
    except Exception as e:
        print 'No hay na'
        dic = {
            'result' : 'OK',
            'precio' : Decimal(0,0)
        }
    response = JsonResponse(dic)
    return HttpResponse(response.content)