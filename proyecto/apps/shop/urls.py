from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns('',
	url(r'^productos/(?P<clave_valor>.*)/$', productos_generales),
	url(r'^productos/$', redirec_prod),
	url(r'^producto_detalle/(\d+)/$', producto_detalle),
	url(r'^ver_carrito/$',ver_carrito),
	url(r'^envio_datos/$',gestionar_compra),
	url(r'^pedidos/$',gestion_pedidos),
	url(r'^ingresar_deposito/$',ingresar_deposito),
	url(r'^pedido/(\d+)/$', pedido_detalle),
	url(r'^consulta_transporte/$',consulta_transporte),
)