var dataCars = localStorage.getItem("dataCars");
var subtotal = localStorage.getItem("subtotal");
var datosContacto = sessionStorage.getItem("datos_cont");
var precioIva = 0.0;
var total_a_pagar = 0.0;
var tarifa_anterior = 0;

dataCars = JSON.parse(dataCars);
datosContacto = JSON.parse(datosContacto);

document.onload = cargarTabla();

function cargarTabla(){

	$("#cuerpo").empty();
    $(".subtotal_carrito").empty();

	for (var i = 0; i < dataCars.length; i++) {
		data = JSON.parse(dataCars[i]);
		$("#cuerpo").append('<tr class="cart_table_item">'+
            '<td ><a class="remove" onclick=borrar_item_carrito('+data.ID+') href="javascript:void(0);">×</a></td>'+
            '<td class="product-thumbnail"><a href="/shop/producto_detalle/'+data.ID+'"><img src="'+data.Imagen+'" alt=""></a></td>'+
            '<td class="product-name"><a href="/shop/producto_detalle/'+data.ID+'">'+data.Nombre+'</a></td>'+
            '<td class="product-price"><span class="amount">$'+data.Precio+'</span></td>'+
            '<td >'+
            '<form class="cart" method="post" action="javascript:void(0);">'+
            '<div class="quantity">'+
            '<a id="'+data.ID+'" class="minus button" onclick="suma_resta(this);" href="javascript:void(0);">-</a>'+
            '<input class="input-text qty text" type="number" title="Qty" value="'+data.Cantidad+'" disabled name="quantity" min="1" step="1">'+
            '<a id="'+data.ID+'" class="plus button" onclick="suma_resta(this);" href="javascript:void(0);">+</a>'+
            '</div>'+
            '<div class="clear"></div>'+
            '</form>'+
            '</td>'+
            '<td class="product-subtotal">'+
            '<span id="total-'+data.ID+'" class="amount">$'+data.Total+'</span>'+
            '</td>'+
            '</tr>');
	}
    var subtotal = actualizar_subtotal();
    actualizar_precio_iva(subtotal);
    $(".subtotal_carrito").append('$'+subtotal);
    cargarTablaPeque();
		cargarDatosAnteriores();
}

function cargarDatosAnteriores(){
		if (datosContacto != null){
				$('#txt_ciudad').val(datosContacto.ciudad);
				$('#txt_cod').val(datosContacto.codigo_postal);
				$('#txt_direccion').val(datosContacto.direccion);
				$('#select_empresa').val(datosContacto.emp_transporte);
		}
}

function cargarTablaPeque(){

    $("#cuerpo_tabla2").empty();

    for (var i = 0; i < dataCars.length; i++) {
        data = JSON.parse(dataCars[i]);
        $("#cuerpo_tabla2").append('<tr class="cart_table_item">'+
            '<td><a href="/shop/producto_detalle/'+data.ID+'">'+data.Nombre+'</a></td>'+
            '<td><span class="amount">$'+data.Precio+'</span></td>'+
            '<td>'+
            '<input class="input-text qty text" type="number" title="Qty" value="'+data.Cantidad+'" disabled name="quantity" min="1" step="1">'+
            '</td>'+
            '<td>'+
            '<span id="total-'+data.ID+'" class="amount">$'+data.Total+'</span>'+
            '</td>'+
            '</tr>');
    }
}

function suma_resta(boton){
    var button = jQuery(boton);
    var id = button.attr('id');
    var oldValue = button.parent().find("input").val();

    if (button.text() == "+") {
        var newVal = parseFloat(oldValue) + 1;
    } else {
        // Don't allow decrementing below zero
        if (oldValue > 1) {
            var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 1;
        }
    }
    button.parent().find("input").val(newVal);
    actualizar_prod(parseInt(id),newVal);
    cargarTablaPeque();
}

function actualizar_prod(id, cantidad){
    var respuesta = null;
    for (var i = 0; i < dataCars.length; i++) {
        datos = JSON.parse(dataCars[i]);
        if(datos.ID === id){
            datos.Cantidad = cantidad;
            datos.Total = parseFloat(datos.Precio) * parseFloat(datos.Cantidad);
            $("#total-"+id).empty();
            $("#total-"+id).append("$"+datos.Total);
            dataCars.push(JSON.stringify(datos));
            dataCars.splice(i,1);//borrando el dato anterior
            localStorage.setItem("dataCars", JSON.stringify(dataCars));
            var subtotal = actualizar_subtotal();
            actualizar_precio_iva(subtotal);
            $(".subtotal_carrito").empty();
            $(".subtotal_carrito").append('$'+subtotal);
            costo_envio($("#select_empresa").val());
            break;
        }else{
            continue;
        }
    }
}

function actualizar_precio_iva(subtotal){
    var iva = $('#iva').val();
    var respuesta = subtotal * (parseFloat(iva)/100);
    respuesta = Math.round(respuesta * 100) / 100;
    precioIva = respuesta;
    total_a_pagar = subtotal + precioIva;
    total_a_pagar = Math.round(total_a_pagar * 100) / 100;
    $(".subtotal_iva").empty();
    $(".subtotal_iva").append('$'+respuesta);
    $(".total_carrito").empty();
    $(".total_carrito").append('$'+total_a_pagar);
}

function borrar_item_carrito(id){
    swal({
        title: "Confirmar acción",
        text: "Desea borrar el producto del carrito?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, por favor",
        cancelButtonText: "No, me equivoque!",
        closeOnConfirm: true,
        closeOnCancel: true,
        html: true
    },
    function(isConfirm){
        if (isConfirm) {
            resp = revisar_si_existe(id);
            if(resp!=null){
                dataCars.splice(resp.indice_prod,1);
            }else{
                console.error('No se ha econtrado el producto');
            }
            localStorage.setItem("dataCars", JSON.stringify(dataCars));
            cargarTabla();
            cargarTablaPeque();
        }
    });
}

function envioData(){
    if(dataCars.length > 0){

        var csrftoken = getCookie('csrftoken');
        var ciudad = $('#txt_ciudad').val();
        var cod_postal = $('#txt_cod').val();
        var direccion = $('#txt_direccion').val();
        var empresa_trans = $('#select_empresa').val();
        swal({
          title: "Confirmación de compra",
          text: "Una vez confirmado la accion puede seguir los pasos en el menu de pedidos.",
          type: "info",
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
      },
      function(){
        $.ajax({
            type: "POST",
            url: "/shop/envio_datos/",
            dataType: 'json',
            data: {
                carrito: dataCars,
                ciudad : ciudad,
                cod_postal : cod_postal,
                direccion : direccion,
                empresa_trans : empresa_trans,
                subtotal : total_a_pagar,
                csrfmiddlewaretoken: csrftoken
            },
            success: function(response) {
                if(response.result==='OK'){
                    localStorage.setItem("dataCars", null);
                    localStorage.setItem("subtotal", null);
                    swal({
                        title: "Muy bien",
                        text: "Puede monitorear el estado de su pedido desde su panel de control.",
                        type: "info",
                        closeOnConfirm: true
                    },
                    function(){
                        location.href = "/shop/pedidos/";
                    });
                }else if(response.result==='LOGEATE'){
                    swal({
                        title: "Necesita Logearse",
                        text: "Para realizar la compra inicie sesion con una cuenta de usuario e intente nuevamente por favor.",
                        type: "warning",
                        closeOnConfirm: true
                    },
                    function(){
												var dat_temp = {
														"ciudad": $("#txt_ciudad").val(),
														"codigo_postal": $("#txt_cod").val(),
														"direccion": $("#txt_direccion").val(),
														"emp_transporte": $('#select_empresa').val(),
												};
												sessionStorage.setItem('datos_cont',JSON.stringify(dat_temp));
                        modalReg();
                    });
                }else if(response.result==='PRODUCTO'){
                    swal("Lo sentimos", response.mensaje, "warning")
                }
                else{
                    console.error('Error de permisos');
                }
            },
            error: function(e) {
                console.error('Hay un error');
                console.log(e.responseText);
            }
        });
    });
    }
}

function costo_envio(valor){
    var csrftoken = getCookie('csrftoken');
    $.ajax({
    type: "POST",
    url: "/shop/consulta_transporte/",
    dataType: 'json',
    data: {
        id_empresa: valor,
        csrfmiddlewaretoken: csrftoken
    },
    success: function(response) {
        if(response.result==='OK'){
            total_a_pagar = total_a_pagar - tarifa_anterior;
            total_a_pagar = total_a_pagar + parseFloat(response.precio);
            tarifa_anterior = parseFloat(response.precio);
            $("#gastos_env").empty();
            $("#gastos_env").append('$'+response.precio);
            $("#total_carrito").empty();
            $("#total_carrito").append('$'+total_a_pagar);
        }
    },
    error: function(e) {
        console.error('Hay un error');
        console.log(e.responseText);
    }
});
}
