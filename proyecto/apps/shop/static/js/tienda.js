function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$('#form-comment').on("submit", function() {
    event.preventDefault();

    var csrftoken = getCookie('csrftoken');
    var msg = $('#mensaje').val();
    var user = $('#usuario').val();
    var pro = $('#pro_id').val();
    var val = $('#valoracion').val();
    if (msg != '') {
        $.ajax({
            type: "POST",
            url: "/shop/agregar-comentario/",
            dataType: 'json',
            data: {
                msg: msg,
                user: user,
                pro: pro,
                val: val,
                csrfmiddlewaretoken: csrftoken
            },
            success: function(response) {
                console.log(response);
                if (response.result == "error") {
                    var men = "daylisoft sufrio un error inesperado, vuelva a intentar.";
                    toastr.options={"progressBar":true};
                    toastr.error(men,'Comentarios');
                }else{
                    
                    $("#bloque_comentarios").load(location.href+" #bloque_comentarios>*","");
                    $('#mensaje').val('');
                    var men = "comentario resgistrado con éxito.";
                    toastr.options={"progressBar":true};
                    toastr.info(men,'Comentarios');
                }
            },
            error: function() {
                var men = "daylisoft sufrio un error inesperado, vuelva a intentar.";
                toastr.options={"progressBar":true};
                toastr.error(men,'Comentarios');
            }
        });
    }else{
        console.error('No se pueden enviar mensajes en blanco');
    }
});