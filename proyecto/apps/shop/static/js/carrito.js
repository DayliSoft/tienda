var dataCars = localStorage.getItem("dataCars");
var subtotal = localStorage.getItem("subtotal");

dataCars = JSON.parse(dataCars); 

if(dataCars == null)
    dataCars = [];

actualizarLista();

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function AddCar(id, precio){
    var cantidad = $("#txtCantidad").val();
    var total = parseFloat(precio.replace(',','.')) * parseInt(cantidad);
    swal({
        title: "Confirmar acción",
        text: "Desea agregar <b>"+cantidad+"</b> "+$("#txtNombre").text()+" al carrito por un valor de: <b>$"+total+"</b>?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#22a1c4",
        confirmButtonText: "Si, por favor",
        cancelButtonText: "No, me equivoque!",
        closeOnConfirm: true,
        closeOnCancel: true,
        html: true
    },
    function(isConfirm){
        if (isConfirm) {
            resp = revisar_si_existe(id);
            if(resp!=null){
                var nueva_cant = parseFloat(cantidad) + parseFloat(resp.cantidad_prod);
                dataCars.splice(resp.indice_prod,1);
                var car = JSON.stringify({
                    ID    : id ,
                    Nombre  : $("#txtNombre").text() ,
                    Precio : parseFloat(precio.replace(',','.')),
                    Cantidad : parseFloat(cantidad) + parseFloat(resp.cantidad_prod),
                    Total : parseFloat(precio.replace(',','.')) * parseInt(nueva_cant),
                    Imagen : $("#largeImage").attr('src')
                });
            }else{
                var car = JSON.stringify({
                    ID    : id ,
                    Nombre  : $("#txtNombre").text() ,
                    Precio : parseFloat(precio.replace(',','.')),
                    Cantidad : cantidad,
                    Total : total,
                    Imagen : $("#largeImage").attr('src')
                });
            }
            dataCars.push(car);
            localStorage.setItem("dataCars", JSON.stringify(dataCars));
            actualizarLista();
        }
    });
}

function AddCarModal(id,precio,nombre,imagen){
    var cantidad = $("#txt_cantidad1").val();
    var total = parseFloat(precio.replace(',','.')) * parseInt(cantidad);
    swal({
        title: "Confirmar acción",
        text: "Desea agregar <b>"+cantidad+"</b> "+nombre+" al carrito por un valor de: <b>$"+total+"</b>?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#22a1c4",
        confirmButtonText: "Si, por favor",
        cancelButtonText: "No, me equivoque!",
        closeOnConfirm: true,
        closeOnCancel: true,
        html: true
    },
    function(isConfirm){
        if (isConfirm) {
            resp = revisar_si_existe(id);
            if(resp!=null){
                var nueva_cant = parseFloat(cantidad) + parseFloat(resp.cantidad_prod);
                dataCars.splice(resp.indice_prod,1);
                var car = JSON.stringify({
                    ID    : id ,
                    Nombre  : nombre,
                    Precio : parseFloat(precio.replace(',','.')),
                    Cantidad : nueva_cant,
                    Total : parseFloat(precio.replace(',','.')) * parseInt(nueva_cant),
                    Imagen : imagen
                });
            }else{
                var car = JSON.stringify({
                    ID    : id ,
                    Nombre  : nombre,
                    Precio : parseFloat(precio.replace(',','.')),
                    Cantidad : cantidad,
                    Total : total,
                    Imagen : imagen
                });
            }
            dataCars.push(car);
            localStorage.setItem("dataCars", JSON.stringify(dataCars));
            actualizarLista();
        }
    });
}

function revisar_si_existe(id){
    var respuesta = null;
    for (var i = 0; i < dataCars.length; i++) {
        datos = JSON.parse(dataCars[i]);
        if(datos.ID === id){
            data = {
                "id_prod" : datos.ID,
                "cantidad_prod" : datos.Cantidad,
                'indice_prod' : i,
            }
            respuesta = data;
            break;
        }else{
            continue;
        }
    }
    return respuesta;
}

function actualizarLista(){
    $("#listaProductos").empty();
    if(dataCars.length > 0){
        for (var i = dataCars.length - 1; i >= 0; i--) {
            datos = JSON.parse(dataCars[i]);
            $("#listaProductos").append('<li>'+
                '<img src="'+datos.Imagen+'" alt="" />'+
                '<a href="/shop/producto_detalle/'+datos.ID+'" class="title">'+datos.Nombre+'</a>'+
                '<div class="price">'+datos.Cantidad+'<span>x</span>$'+datos.Precio+'</div>'+
                '</li>');
        }
        var subtotal = actualizar_subtotal();
        $("#subtotal").empty();
        $("#subtotal").append('$'+subtotal);
    }
}

function actualizar_subtotal(){
    var subtotal = 0.0;
    if(dataCars != null){
        for (var i = dataCars.length - 1; i >= 0; i--) {
            datos = JSON.parse(dataCars[i]);
            subtotal = subtotal + datos.Total;
            subtotal = Math.round(subtotal * 100) / 100;
        }
        localStorage.setItem("subtotal", subtotal);
    }
    return subtotal;
}