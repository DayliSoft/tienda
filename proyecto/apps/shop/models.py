from django.db import models
from django.conf import settings
import datetime
from mptt.models import MPTTModel, TreeForeignKey
from django.db import connection
import cx_Oracle
from apps.users.models import User

# Create your models here.
class marca(models.Model):
    mar_id = models.AutoField(primary_key=True)
    mar_nombre = models.CharField(max_length=50)

    def new_marca(self, mar_nombre) :
        cursor = connection.cursor()
        ret = cursor.callproc("new_marca", (mar_nombre,0))
        cursor.close()
        return ret
    def update_marca(self, mar_id, mar_nombre):
        cursor = connection.cursor()
        ret = cursor.callproc("update_marca", (mar_id, mar_nombre))
        cursor.close()
        return ret
    def remove_marca(self, mar_id):
        cursor = connection.cursor()
        ret = cursor.callproc("remove_marca", (mar_id, 0))
        cursor.close()
        return ret
    def view_marca(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_marca",cx_Oracle.CURSOR)
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'mar_id':i[0], 'mar_nombre': i[1]})
        return datosQuery
    def view_marca_nombre(self, mar_nombre):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_marca_nombre",cx_Oracle.CURSOR, (0, mar_nombre))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append([i[0],i[1]])
        return datosQuery
    def view_marca_id(self, mar_id):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_marca_id",cx_Oracle.CURSOR, (mar_id, 0))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'mar_id':i[0], 'mar_nombre': i[1]})
        return datosQuery
    def __unicode__(self):
        return self.mar_nombre

    class Meta:
        verbose_name_plural='Marcas'
        app_label = 'shop'

class categoria(models.Model):
    cat_id = models.AutoField(primary_key=True)
    cat_nombre = models.CharField(max_length=30)
    cat_descripcion = models.CharField(max_length=300)
    def new_categoria(self, cat_nombre,cat_descripcion) :
        cursor = connection.cursor()
        ret = cursor.callproc("new_categoria", (cat_nombre,cat_descripcion))
        cursor.close()
        return ret
    def update_categoria(self, cat_id, cat_nombre, cat_descripcion):
        cursor = connection.cursor()
        ret = cursor.callproc("update_categoria", (cat_id, cat_nombre, cat_descripcion))
        cursor.close()
        return ret
    def remove_categoria(self, cat_id):
        cursor = connection.cursor()
        ret = cursor.callproc("remove_categoria", (cat_id, 0))
        cursor.close()
        return ret
    def view_categoria(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_categoria",cx_Oracle.CURSOR)
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'cat_id':i[0], 'cat_nombre': i[1],
                                'cat_descripcion': i[2]})
        return datosQuery
    def view_categoria_nombre(self, cat_nombre):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_categoria_nombre",cx_Oracle.CURSOR, (0, cat_nombre))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append([i[0],i[1], i[2]])
        return datosQuery
    def view_categoria_id(self, cat_id):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_categoria_id",cx_Oracle.CURSOR, (cat_id, 0))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'cat_id':i[0], 'cat_nombre': i[1], 'cat_descripcion': i[2]})
        return datosQuery

    def __unicode__(self):
        return self.cat_nombre

    class Meta:
        verbose_name_plural='Categorias'
        app_label = 'shop'

class producto(models.Model):
    pro_id = models.AutoField(primary_key=True)
    pro_nombre = models.CharField(max_length=50)
    pro_precio_compra = models.DecimalField(max_digits=10,decimal_places=2)
    pro_precio = models.DecimalField(max_digits=10,decimal_places=2)
    pro_descripcion_corta = models.CharField(max_length=360)#cosas consisas como el peso, medidas y esas cosas
    pro_descripcion_larga = models.TextField()#toda la descripcion posible
    pro_info_adicional = models.CharField(max_length=200)
    pro_estado = models.BooleanField(default=True)
    pro_stock = models.IntegerField()
    pro_stock_minimo = models.IntegerField()
    pro_destacado = models.BooleanField(default=False)#solo pueden haber 3 destacados
    pro_valoracion = models.DecimalField(max_digits=10,decimal_places=2)
    categorias = models.ForeignKey(categoria)
    mar_id = models.ForeignKey(marca)
    tags = models.ManyToManyField('tag',related_name='pro_tags')
    def new_producto_p1(self, pro_nombre,pro_precio, pro_descripcion_corta, pro_descripcion_larga,
                            pro_estado , pro_stock, mar_id, pro_info_adicional, pro_stock_minimo, categorias, pro_destacado, pro_valoracion, pro_precio_compra) :
        cursor = connection.cursor()
        ret = cursor.callproc("new_producto_p1", (pro_nombre,pro_precio, pro_descripcion_corta, pro_descripcion_larga,
                            pro_estado , pro_stock, mar_id, pro_info_adicional, pro_stock_minimo, categorias, pro_destacado, pro_valoracion, pro_precio_compra))
        cursor.close()
        return ret
    def update_producto_p1(self, pro_id, pro_nombre, pro_descripcion_corta, pro_descripcion_larga,
                            pro_estado , mar_id, pro_info_adicional, categorias):
        cursor = connection.cursor()
        ret = cursor.callproc("update_producto_p1", (pro_id, pro_nombre, pro_descripcion_corta, pro_descripcion_larga,
                            pro_estado , mar_id, pro_info_adicional, categorias))
        cursor.close()

    def update_producto_p2(self, pro_id, pro_precio_compra, pro_precio, pro_stock, pro_stock_minimo):
        cursor = connection.cursor()
        ret = cursor.callproc("update_producto_p2", (pro_id, pro_precio_compra, pro_precio, pro_stock, pro_stock_minimo))
        cursor.close()
        return ret

    def update_producto_p3(self, pro_id, pro_destacado, pro_valoracion):
        cursor = connection.cursor()
        ret = cursor.callproc("update_producto_p3", (pro_id, pro_destacado, pro_valoracion))
        cursor.close()
        return ret

    def new_tags_productos(self, tag_id, prod_id):
        cursor = connection.cursor()
        ret = cursor.callproc("new_tags_productos", (tag_id, prod_id))
        cursor.close()
        return ret
    def delete_producto_imagen(self, prod_id):
        cursor = connection.cursor()
        ret = cursor.callproc("delete_producto_imagen", (prod_id, 0))
        cursor.close()
        return ret
    def delete_producto_tags(self, prod_id):
        cursor = connection.cursor()
        ret = cursor.callproc("delete_producto_tags", (prod_id, 0))
        cursor.close()
        return ret
    def remove_producto(self, prod_id):
        cursor = connection.cursor()
        ret = cursor.callproc("remove_producto", (prod_id, 0))
        cursor.close()
        return ret
    def view_producto_nombre(self, pro_nombre):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_producto_nombre",cx_Oracle.CURSOR, (0, pro_nombre))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append([i[0]])
        return datosQuery
    def new_imagen_producto(self, img_data, id_prod):
        ima = imagen_producto(img_data=img_data, img_es_principal=True, pro_id_id=id_prod)
        ima.save()
    def view_producto(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_producto",cx_Oracle.CURSOR)
        cursor.close()
        datosQuery=[]
        imagen='nada'
        for i in ret:
            cursor = connection.cursor()
            ret1 = cursor.callfunc("view_producto_imagen",cx_Oracle.CURSOR, (i[0], 0))
            cursor.close()
            for j in ret1:
                imagen=j[1]
            datosQuery.append({'pro_id':i[0], 'pro_nombre': i[1], 'pro_precio': i[2],
                    'pro_descripcion_corta': i[3], 'pro_descripcion_larga': i[4],
                    'pro_estado': i[5], 'pro_stock': i[6],
                    'mar_id': i[7], 'pro_info_adicional': i[8],
                    'pro_stock_minimo': i[9], 'categorias': i[10],
                    'pro_destacado': i[11], 'pro_valoracion': i[12],
                    'pro_precio_compra': i[13], 'pro_imagen':imagen})
        return datosQuery
    def view_producto_p1_id(self, id_pro):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_producto_p1_id",cx_Oracle.CURSOR, (id_pro, 0))
        cursor.close()
        datosQuery=[]
        imagen='nada'
        for i in ret:
            datosQuery.append({'pro_id':i[0], 'pro_nombre': i[1], 'pro_precio': i[2],
                    'pro_descripcion_corta': i[3], 'pro_descripcion_larga': i[4],
                    'pro_estado': i[5], 'pro_stock': i[6],
                    'mar_id': i[7], 'pro_info_adicional': i[8],
                    'pro_stock_minimo': i[9], 'categorias': i[10],
                    'pro_destacado': i[11], 'pro_valoracion': i[12],
                    'pro_precio_compra': i[13], 'pro_imagen':imagen})
        return datosQuery
    def view_producto_id(self, id_pro):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_producto_id",cx_Oracle.CURSOR, (id_pro, 0))
        cursor.close()
        datosQuery=[]
        imagen='nada'
        for i in ret:
            cursor = connection.cursor()
            ret1 = cursor.callfunc("view_producto_imagen",cx_Oracle.CURSOR, (i[0], 0))
            cursor.close()
            for j in ret1:
                imagen=j[1]
            datosQuery.append({'pro_id':i[0], 'pro_nombre': i[1], 'pro_precio': i[2],
                    'pro_descripcion_corta': i[3], 'pro_descripcion_larga': i[4],
                    'pro_estado': i[5], 'pro_stock': i[6],
                    'mar_id': i[7], 'pro_info_adicional': i[8],
                    'pro_stock_minimo': i[9], 'categorias': i[10],
                    'pro_destacado': i[11], 'pro_valoracion': i[12],
                    'pro_precio_compra': i[13], 'pro_imagen':imagen})
        return datosQuery
    def view_producto_exclude_estado(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_producto_exclude_estado",cx_Oracle.CURSOR)
        cursor.close()
        datosQuery=[]
        imagen='nada'
        for i in ret:
            cursor = connection.cursor()
            ret1 = cursor.callfunc("view_producto_imagen",cx_Oracle.CURSOR, (i[0], 0))
            cursor.close()
            for j in ret1:
                imagen=j[1]
            datosQuery.append({'pro_id':i[0], 'pro_nombre': i[1], 'pro_precio': i[2],
                    'pro_descripcion_corta': i[3], 'pro_descripcion_larga': i[4],
                    'pro_estado': i[5], 'pro_stock': i[6],
                    'mar_id': i[7], 'pro_info_adicional': i[8],
                    'pro_stock_minimo': i[9], 'categorias': i[10],
                    'pro_destacado': i[11], 'pro_valoracion': i[12],
                    'pro_precio_compra': i[13], 'pro_imagen':imagen})
        return datosQuery
    def view_producto_by_filter(self, query):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_producto_by_filter",cx_Oracle.CURSOR, (0, query))
        cursor.close()
        datosQuery=[]
        imagen='nada'
        for i in ret:
            cursor = connection.cursor()
            ret1 = cursor.callfunc("view_producto_imagen",cx_Oracle.CURSOR, (i[0], 0))
            cursor.close()
            for j in ret1:
                imagen=j[1]
            datosQuery.append({'pro_id':i[0], 'pro_nombre': i[1], 'pro_precio': i[2],
                    'pro_descripcion_corta': i[3], 'pro_descripcion_larga': i[4],
                    'pro_estado': i[5], 'pro_stock': i[6],
                    'mar_id': i[7], 'pro_info_adicional': i[8],
                    'pro_stock_minimo': i[9], 'categorias': i[10],
                    'pro_destacado': i[11], 'pro_valoracion': i[12],
                    'pro_precio_compra': i[13], 'pro_imagen':imagen})
        return datosQuery
    def __unicode__(self):
        return self.pro_nombre

    class Meta:
        verbose_name_plural='Productos'
        app_label = 'shop'

class tag(models.Model):
    tag_id = models.AutoField(primary_key=True)
    tag_nombre = models.CharField(max_length=20)

    def view_tag_producto_id(self, id_pro):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_tag_producto_id",cx_Oracle.CURSOR, (id_pro, 0))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'tag_id':i[0], 'tag_nombre': i[1]})
        return datosQuery
    def view_tag(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_tag",cx_Oracle.CURSOR)
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'tag_id':i[0], 'tag_nombre': i[1]})
        return datosQuery
    def __unicode__(self):
        return self.tag_nombre

    class Meta:
        verbose_name_plural='Tags'
        app_label = 'shop'

class emp_trasporte(models.Model):
    trans_id = models.AutoField(primary_key=True)
    trans_nombre = models.CharField(max_length=50)
    trans_costo  = models.DecimalField(max_digits=10,decimal_places=2)
    trans_url = models.CharField(max_length=50)

    def new_transporte(self, trans_nombre, trans_costo, trans_url) :
        cursor = connection.cursor()
        ret = cursor.callproc("new_transporte", (trans_nombre, trans_costo, trans_url))
        cursor.close()
        return ret
    def update_transporte(self, trans_id, trans_nombre, trans_costo, trans_url):
        cursor = connection.cursor()
        ret = cursor.callproc("update_transporte", (trans_id, trans_nombre, trans_costo, trans_url))
        cursor.close()
        return ret
    def remove_transporte(self, trans_id):
        cursor = connection.cursor()
        ret = cursor.callproc("remove_transporte", (trans_id, 0))
        cursor.close()
        return ret
    def view_transporte(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_transporte",cx_Oracle.CURSOR)
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'trans_id':i[0], 'trans_nombre': i[1], 'trans_costo':i[2], 'trans_url':i[3]})
        return datosQuery

    def view_transporte_nombre(self, trans_nombre):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_transporte_nombre",cx_Oracle.CURSOR, (0, trans_nombre))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append([i[0],i[1],i[2], i[3]])
        return datosQuery

    def view_transporte_id(self, trans_id):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_transporte_id",cx_Oracle.CURSOR, (trans_id, 0))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'trans_id':i[0], 'trans_nombre': i[1], 'trans_costo':i[2], 'trans_url':i[3]})
        return datosQuery

    def __unicode__(self):
        return self.trans_nombre

    class Meta:
        verbose_name_plural='Transportes'
        app_label = 'shop'

class pedido(models.Model):
    ped_id = models.AutoField(primary_key=True)
    cliente = models.ForeignKey(settings.AUTH_USER_MODEL)
    ped_fecha_pedido = models.DateTimeField(auto_now_add=True)
    ped_fecha_envio = models.CharField(max_length=150, null=True,blank=True)
    ped_direccion_envio = models.CharField(max_length=300)
    ped_ciudad_envio = models.CharField(max_length=50)
    ped_cod_postal_envio = models.CharField(max_length=10)
    ped_pais_envio = models.CharField(max_length=20)
    ped_codigo_guia = models.CharField(max_length=50, null=True, blank=True)
    ped_estado = models.CharField(max_length=20,default='pendiente')#cancelado, pendiente, verificacion, enviado
    ped_cod_deposito = models.CharField(max_length=30, null=True, blank=True)
    ped_imagen_deposito = models.ImageField(upload_to = 'depositos', null=True,blank=True,default='/static/daylisoft/img/sin_imagen.gif')
    trans_id = models.ForeignKey(emp_trasporte, null=True, blank=True)
    ped_total_pagar = models.DecimalField(max_digits=10,decimal_places=2)

    def new_pedido(self, ped_cliente, ped_direccion_envio, ped_ciudad_envio,ped_cod_postal_envio,
                         ped_pais_envio, trans_id, ped_total_pagar):
        cursor = connection.cursor()
        ret = cursor.callfunc("new_pedido_func",cx_Oracle.CURSOR, (ped_cliente, ped_direccion_envio, ped_ciudad_envio,ped_cod_postal_envio,ped_pais_envio, trans_id, ped_total_pagar))
        cursor.close()
        return ret
    def update_pedido(self, ped_id, fecha, estado, guia):
        cursor = connection.cursor()
        ret = cursor.callproc("update_pedido", (ped_id, fecha, estado, guia))
        cursor.close()
        return ret
    def update_deposito(self, ped_id, numero_deposito, estado, imagen):
        cursor = connection.cursor()
        ret = cursor.callproc("update_deposito", (ped_id, numero_deposito, estado))
        cursor.close()
        pedidoG=pedido.objects.get(ped_id=ped_id)
        pedidoG.ped_imagen_deposito=imagen
        pedidoG.save()
        return ret
    def remove_pedido(self, ped_id):
        cursor = connection.cursor()
        ret = cursor.callproc("remove_pedido", (ped_id, 0))
        cursor.close()
        return ret
    def view_pedido(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_pedido",cx_Oracle.CURSOR)
        cursor.close()
        datosQuery=[]
        for i in ret:
            tra=''
            if((i[8])!=None):
                tra=emp_trasporte.objects.get(trans_id=i[8]).trans_nombre
            datosQuery.append({'ped_id':i[0],
                'ped_fecha_pedido': i[1],
                 'ped_fecha_envio':i[2],
                 'ped_direccion_envio':i[3],
                 'ped_ciudad_envio':i[4],
                 'ped_cod_postal_envio':i[5],
                 'ped_pais_envio':i[6],
                 'cliente':User.objects.get(pk=i[7]),
                 'cliente_id':i[7],
                 'trans_id':i[8],
                 'trans_nombre':tra,
                 'ped_cod_deposito':i[9],
                 'ped_codigo_guia':i[10],
                 'ped_estado':i[11],
                 'ped_imagen_deposito':i[12],
                 'ped_total_pagar':i[13]})
        return datosQuery
    def view_pedido_filter(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_pedido_filter",cx_Oracle.CURSOR)
        cursor.close()
        datosQuery=[]
        for i in ret:
            tra=''
            if((i[8])!=None):
                tra=emp_trasporte.objects.get(trans_id=i[8]).trans_nombre
            datosQuery.append({'ped_id':i[0],
                'ped_fecha_pedido': i[1],
                 'ped_fecha_envio':i[2],
                 'ped_direccion_envio':i[3],
                 'ped_ciudad_envio':i[4],
                 'ped_cod_postal_envio':i[5],
                 'ped_pais_envio':i[6],
                 'cliente':User.objects.get(pk=i[7]),
                 'cliente_id':i[7],
                 'trans_id':i[8],
                 'trans_nombre':tra,
                 'ped_cod_deposito':i[9],
                 'ped_codigo_guia':i[10],
                 'ped_estado':i[11],
                 'ped_imagen_deposito':i[12],
                 'ped_total_pagar':i[13]})
        return datosQuery
    def view_pedido_cliente(self, cli_id):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_pedido_cliente",cx_Oracle.CURSOR, (0, cli_id))
        cursor.close()
        datosQuery=[]
        for i in ret:
            tra=''
            if((i[8])!=None):
                tra=emp_trasporte.objects.get(trans_id=i[8]).trans_nombre
            datosQuery.append({'ped_id':i[0], 'ped_fecha_pedido': i[1],
             'ped_fecha_envio':i[2],
             'ped_direccion_envio':i[3].read(),
             'ped_ciudad_envio':i[4],
             'ped_cod_postal_envio':i[5],
             'ped_pais_envio':i[6],
             'cliente':i[7],
             'trans_nombre':tra,
             'trans_id':i[8],
             'ped_cod_deposito':i[9],
             'ped_codigo_guia':i[10],
             'ped_estado':i[11],
             'ped_imagen_deposito':i[12],
             'ped_total_pagar':i[13],
             })
        return datosQuery
    def view_pedido_id(self, ped_id):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_pedido_id",cx_Oracle.CURSOR, (0, ped_id))
        cursor.close()
        datosQuery=[]
        for i in ret:
            tra=''
            if((i[8])!=None):
                tra=emp_trasporte.objects.get(trans_id=i[8]).trans_nombre
            datosQuery.append({'ped_id':i[0], 'ped_fecha_pedido': i[1],
             'ped_fecha_envio':i[2],
             'ped_direccion_envio':i[3].read(),
             'ped_ciudad_envio':i[4],
             'ped_cod_postal_envio':i[5],
             'ped_pais_envio':i[6],
             'cliente':User.objects.get(pk=i[7]),
             'cliente_id':i[7],
             'trans_nombre':tra,
             'trans_id':i[8],
             'ped_cod_deposito':i[9],
             'ped_codigo_guia':i[10],
             'ped_estado':i[11],
             'ped_imagen_deposito':i[12],
             'ped_total_pagar':i[13],
             })
        return datosQuery
    def __unicode__(self):
        return self.cliente.us_nombre+'-'+str(self.ped_fecha_pedido)

    class Meta:
        verbose_name_plural='Pedidos'
        app_label = 'shop'

class det_pedido(models.Model):
    pro_id = models.ForeignKey(producto)
    ped_id = models.ForeignKey(pedido)
    precio_unit = models.DecimalField(max_digits=10,decimal_places=2)
    cantidad = models.IntegerField()
    descuento = models.DecimalField(max_digits=10,decimal_places=2)
    total_pagar = models.DecimalField(max_digits=10,decimal_places=2)


    def new_detalle_pedido(self, pro_id,ped_id,precio_unit,cantidad,descuento, total_pagar ):
        cursor = connection.cursor()
        ret = cursor.callproc("new_detalle_pedido", (pro_id,ped_id,precio_unit,cantidad,descuento, total_pagar ))
        cursor.close()
        return ret

    def view_detalle_pedido_id(self, ped_id):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_detalle_pedido_id",cx_Oracle.CURSOR, (0, ped_id))
        cursor.close()
        datosQuery=[]
        for i in ret:
            datosQuery.append({'id':i[0], 'precio_unit': i[1],
             'cantidad':i[2],
             # 'ped_direccion_envio':i[3],
             'descuento':i[3],
             'total_pagar':i[4],
             'ped_id':i[5],
             'pro_id':i[6],
             'pro_nombre':producto().view_producto_id(i[6])[0]['pro_nombre']
             })
        return datosQuery
    class Meta:
        verbose_name_plural='Detalle pedidos'
        app_label = 'shop'

class imagen_producto(models.Model):
    img_id = models.AutoField(primary_key=True)
    img_data = models.ImageField(upload_to = 'imagenes_productos')
    img_es_principal = models.BooleanField(default=False)
    pro_id = models.ForeignKey(producto)

    def __unicode__(self):
        return self.pro_id.pro_nombre

    class Meta:
        verbose_name_plural='Imagenes productos'
        app_label = 'shop'
