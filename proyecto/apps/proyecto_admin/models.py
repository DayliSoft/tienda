from django.db import models
from django.conf import settings
import datetime
from django.db import connection
import cx_Oracle

class perfil_empresa(models.Model):
    per_id = models.AutoField(primary_key=True)
    per_nombre = models.CharField(max_length=100)
    per_email = models.CharField(max_length=100)
    per_direccion = models.CharField(max_length=250)
    per_telefono = models.CharField(max_length=30)
    per_celular = models.CharField(max_length=30)
    per_logo= models.ImageField(upload_to='logos_empresas')
    per_ruc = models.CharField(max_length=20)

    def __unicode__(self):
        return self.per_nombre

    class Meta:
        verbose_name_plural=u'Perfiles'
        app_label = 'proyecto_admin'

class impuesto(models.Model):
    imp_id = models.AutoField(primary_key=True)
    imp_nombre = models.CharField(max_length=100)
    imp_porcentaje = models.FloatField(blank=False, null=False, default=0)
    imp_actual = models.BooleanField(default=False)
    def new_impuesto(self, imp_nombre, imp_porcentaje, imp_actual) :
        cursor = connection.cursor()
        ret = cursor.callproc("new_impuesto", (imp_nombre, imp_porcentaje, imp_actual))
        cursor.close()
        return ret
    def update_impuesto(self, imp_id, imp_nombre, imp_porcentaje):
        cursor = connection.cursor()
        ret = cursor.callproc("update_impuesto", (imp_id, imp_nombre, imp_porcentaje))
        cursor.close()
        return ret
    def update_impuesto_campo(self, imp_id, imp_actual):
        cursor = connection.cursor()
        ret = cursor.callproc("update_impuesto_campo", (imp_id, imp_actual))
        cursor.close()

        return ret
    def remove_impuesto(self, imp_id):
        cursor = connection.cursor()
        ret = cursor.callproc("remove_impuesto", (imp_id, 0))
        cursor.close()
        return ret
    def view_impuesto(self):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_impuesto",cx_Oracle.CURSOR)
        cursor.close()
        return ret
    def view_impuesto_nombre(self, codigo, imp_nombre):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_impuesto_nombre",cx_Oracle.CURSOR, (codigo, imp_nombre))
        cursor.close()
        return ret
    def view_impuesto_id(self, imp_id, codigo):
        cursor = connection.cursor()
        ret = cursor.callfunc("view_impuesto_id",cx_Oracle.CURSOR, (imp_id, codigo))
        cursor.close()
        return ret

    def __unicode__(self):
        return self.Nombre
    def __unicode__(self):
        return self.imp_nombre

    class Meta:
        verbose_name_plural=u'Impuestos'
        app_label = 'proyecto_admin'