from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns('',
    url(r'^$', inicio, name="url_inicio"),

    url(r'^productos/$', listar_productos, name="url_listar_productos"),
    url(r'^productos/agregar/$', form_agregar_producto, name="url_form_agregar_producto"),
    url(r'^productos/agregar/(?P<id_prod>\d+)/$', form_editar_producto, name="url_form_editar_producto"),
    url(r'^productos/add_1', agregar_productos_p1, name="url_agregar_productos_p1"),
    url(r'^productos/add_2', agregar_productos_p2, name="url_agregar_productos_p2"),
    url(r'^productos/add_3', agregar_productos_p3, name="url_agregar_productos_p3"),
    url(r'^productos/add_4', agregar_productos_p4, name="url_agregar_productos_p4"),
    url(r'^productos/eliminar', eliminar_productos, name="url_eliminar_productos"),
    url(r'^productos/filtrar', filtrar_nombre_producto, name="url_filtrar_nombre_producto"),

    url(r'^productos/categoria/$', listar_categorias_tabla, name="url_listar_categorias_tabla"),
    url(r'^productos/categorias/select', cargar_categorias_select, name="url_cargar_categorias_select"),
    url(r'^productos/categorias/agregar', agregar_categoria, name="url_agregar_categoria"),
    url(r'^productos/categorias/cargar', cargar_categoria, name="url_cargar_categoria"),
    url(r'^productos/categorias/eliminar', eliminar_categoria, name="url_eliminar_categoria"),

    url(r'^productos/marcas/$', listar_marcas, name="url_listar_marcas"),
    url(r'^productos/marcas/agregar', agregar_marca, name="url_agregar_marca"),
    url(r'^productos/marcas/cargar', cargar_marca, name="url_cargar_marca"),
    url(r'^productos/marcas/eliminar', eliminar_marca, name="url_eliminar_marca"),

    url(r'^productos/imagenes/$', listar_imagenes, name="url_listar_imagenes"),
    url(r'^productos/imagenes/eliminar', eliminar_imagenes, name="url_eliminar_imagenes"),
    url(r'^productos/imagenes/cambiar', cambiar_imagenes, name="url_cambiar_imagenes"),
    url(r'^productos/imagenes/subir', subir_nueva_imagen, name="url_subir_nueva_imagen"),

    url(r'^clientes/$', listar_clientes, name="url_listar_clientes"),
    url(r'^clientes/agregar/$', form_agregar_cliente, name="url_form_agregar_cliente"),
    url(r'^clientes/agregar/(?P<id_cli>\d+)/$', form_editar_cliente, name="url_form_editar_cliente"),
    url(r'^clientes/add', agregar_cliente, name="url_agregar_cliente"),
    url(r'^clientes/eliminar', eliminar_clientes, name="url_eliminar_clientes"),
    url(r'^clientes/activar', activar_clientes, name="url_activar_clientes"),
    url(r'^clientes/superusuario', superusuario_cliente, name="url_superusuario_cliente"),
    url(r'^clientes/cambio', cambio_clave_cliente, name="url_cambio_clave_cliente"),

    url(r'^transporte/$', listar_transporte, name="url_listar_transporte"),
    url(r'^transporte/agregar', agregar_transporte, name="url_agregar_transporte"),
    url(r'^transporte/cargar', cargar_transporte, name="url_cargar_transporte"),
    url(r'^transporte/eliminar', eliminar_transporte, name="url_eliminar_transporte"),

    url(r'^impuesto/$', listar_impuestos, name="url_listar_impuestos"),
    url(r'^impuesto/agregar', agregar_impuestos, name="url_agregar_impuestos"),
    url(r'^impuesto/cargar', cargar_impuestos, name="url_cargar_impuestos"),
    url(r'^impuesto/eliminar', eliminar_impuestos, name="url_eliminar_impuestos"),
    url(r'^impuesto/actual', impuesto_actual, name="url_impuesto_actual"),

    url(r'^pedidos/$', listar_pedidos, name="url_listar_pedidos"),
    url(r'^notificaciones/pedidos/$', listar_pedidos_pendientes, name="url_listar_pedidos_pendientes"),
    url(r'^pedidos/(?P<id_ped>\d+)/$', ver_detalle_pedido, name="url_ver_detalle_pedido"),
    url(r'^pedidos/deposito', cargar_deposito, name="url_cargar_deposito"),
    url(r'^pedidos/cliente', cargar_ped_cliente, name="url_cargar_ped_cliente"),
    url(r'^pedidos/estado', cambiar_estado_pedidos, name="url_cambiar_estado_pedidos"),
    url(r'^pedidos/eliminar', eliminar_pedido, name="url_eliminar_pedido"),

    url(r'^tags/', cargar_todos_tags, name="url_cargar_todos_tags"),

    url(r'^consulta/productos/', json_productos, name="url_json_productos"),
)