#-*- coding: utf-8 -*-
from django.utils.translation import gettext
from django.core.urlresolvers import reverse
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponseRedirect, HttpResponse
import json
from apps.shop.models import *
from django.db.models import Sum, F, Prefetch, Q
from .models import *
from apps.users.models import User
from django.db import transaction
import os
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate

# Pagina de inicio
def inicio(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    datosQuery=User()
    clientesV=[]
    for clientes in datosQuery.view_user():
        if(clientes['is_superuser']!=1):
            clientesV.append(clientes)
    dic = {
        'productos': producto().view_producto(),
        'pedidos': pedido().view_pedido(),
        'clientes': clientesV
    }

    return render(request, "dashboard/inicio.html", dic)

# Productos ************************************************************************************************************
def form_agregar_producto(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    dic = {
        'categorias': categoria().view_categoria(),
        'marcas': marca().view_marca(),
    }
    return render(request, "dashboard/productos/agregar.html", dic)

def form_editar_producto(request, id_prod):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    registro=producto()
    producto_editar = None
    pro_tag = None

    if id_prod:
        if registro.view_producto_p1_id(id_prod)>0 :
            producto_editar=registro.view_producto_id(id_prod)
            pro_tag= tag().view_tag_producto_id(id_prod)
        else:
            return HttpResponseRedirect(reverse("dashboard_app:url_listar_productos"))

    dic = {
        'producto_editar': producto_editar,
        # 'num_imagenes': (3 - producto_editar[0].imagen_producto_set.count()),
        'pro_tag': pro_tag,
        'categorias': categoria().view_categoria(),
        'marcas': marca().view_marca()
    }
    return render(request, "dashboard/productos/agregar.html", dic)

def listar_productos(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    producto_detalle = None
    tags_detalle = None
    produ=producto().view_producto()
    dic = {
        'productos': produ,
        'producto_detalles': producto_detalle
    }
    return render(request, "dashboard/productos/administrar.html", dic)

def agregar_productos_p1(request):
    datos = {}
    nombre = request.POST.get('nombre_producto')
    categoria = request.POST.get('categoria_producto')
    marca = request.POST.get('id_marca_producto')
    estado = request.POST.get('estado_producto', False)
    d_corta = request.POST.get('descripcion_producto_corta')
    d_larga = request.POST.get('descripcion_producto_larga', gettext("Ninguna").decode("utf-8"))
    informacion = request.POST.get('informacion_ad_producto', gettext("Ninguna").decode("utf-8"))
    registro=producto()
    datosQuery=[]
    datosQuery1=[]
    mensaje = gettext("Cambios guardados correctamente").decode("utf-8")
    id_prod = request.POST.get('id_producto')
    if(estado):
        estado=1
    else:
        estado=0
    try:
        if not id_prod:
            try:
                datosQuery=registro.new_producto_p1(nombre, 0, d_corta, d_larga,
                                                    estado, 0, marca, informacion, 0, categoria, 1, 0, 0)
            except Exception as e:
                print(e)
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")
        else:
            if registro.view_producto_id(id_prod)<=0:
                raise Exception("Error", "No existe el producto")
            registro.update_producto_p1(id_prod,nombre,d_corta,d_larga,estado,marca,informacion,categoria)

            # prod.save()
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")
        datosQuery1=registro.view_producto_nombre(nombre)
        datos['message'] = mensaje
        datos['id'] = datosQuery1[0][0]
        datos['result'] = "OK"
    except Exception as exc:
        print(exc)
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def agregar_productos_p2(request):
    datos = {}
    compra = request.POST.get('precio_compra_producto')
    venta = request.POST.get('precio_venta_producto')
    stock = request.POST.get('stock_producto')
    stock_min = request.POST.get('stock_min_producto')

    mensaje = gettext("Cambios guardados correctamente").decode("utf-8")
    id_prod = request.POST.get('id_producto')
    registro=producto()
    try:
        if registro.view_producto_id(id_prod)<=0:
            raise Exception("Error", "No existe el producto")
        registro.update_producto_p2(id_prod, compra, venta, stock, stock_min)

        datos['message'] = mensaje
        datos['id'] = id_prod
        datos['result'] = "OK"
    except Exception as exc:
        print(exc)
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def agregar_productos_p3(request):
    datos = {}
    return HttpResponse(json.dumps(datos), content_type="application/json")

@transaction.atomic()
def agregar_productos_p4(request):
    datos = {}
    imagenes = request.FILES.getlist('id_imagenes_producto[]')
    destacado = request.POST.get('producto_destacado', False)
    valoracion = request.POST.get('producto_valoracion', 0)

    str_etiquetas = request.POST.get('tags_producto')
    etiquetas = str(str_etiquetas).split(",")

    mensaje = gettext("Cambios guardados correctamente").decode("utf-8")
    id_prod = request.POST.get('id_producto')
    id_imagen = request.POST.get('producto_id_imagen')
    id_radio_imagen_principal = request.POST.get('imagen_principal')

    bandera_imagen = 0
    transaccion = transaction.savepoint()
    registro=producto()
    try:
        if registro.view_producto_id(id_prod)<=0:
            raise Exception("Error", "No existe el producto")
        if(destacado=='on'):
            destacado=1
        else:
            destacado=0
        registro.update_producto_p3(id_prod,destacado,valoracion)
        #borrar la imagen
        print(registro.delete_producto_imagen(id_prod))
        # Guardar la imagen y pone principal
        if imagenes:
            for i in imagenes:
                registro.new_imagen_producto(i, id_prod)
        registro.delete_producto_tags(id_prod)
        for j in etiquetas:
            registro.new_tags_productos(j, id_prod)

        datos['message'] = mensaje
        datos['id'] = id_prod
        datos['result'] = "OK"

        transaction.savepoint_commit(transaccion)
    except Exception as exc:
        print(exc)
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"
        transaction.savepoint_rollback(transaccion)

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_productos(request):
    datos = {}
    id_prod = request.GET.get("id")
    try:

        registro=producto()
        registro.remove_producto(id_prod)

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Categorias y subcategorias *******************************************************************************************
def listar_categorias_tabla(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    registro=categoria()
    datosQuery=[]
    datosQuery=registro.view_categoria()
    dic = {
        'categorias': datosQuery
    }
    return render(request, "dashboard/ajustes/categorias.html", dic)

def agregar_categoria(request):
    datos = {}
    nombre = request.POST.get('id_categoria_hijo')
    descripcion = request.POST.get('categoria_descripcion')
    id_categ = request.POST.get('categoria_id')
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")
    registro=categoria()
    datosQuery=[]
    datosQuery1=[]
    try:
        if not id_categ:
            try:
                datosQuery=registro.new_categoria(nombre, descripcion)
            except Exception as e:
                print(e)
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")
        else:
            datosQuery=[]
            datosQuery=registro.update_categoria(id_categ, nombre, descripcion)
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        datosQuery1=registro.view_categoria_nombre(nombre)
        print(datosQuery1)
        datos['message'] = mensaje
        datos['id'] = datosQuery1[0][0]
        datos['nombre'] =  datosQuery1[0][1]

        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_categoria(request):
    datos = {}
    id_categ = request.GET.get("id")
    datosQuery1=[]
    registro=categoria()
    try:
        datosQuery1=registro.view_categoria_id(id_categ)
        datos['id'] = datosQuery1[0]['cat_id']
        datos['nombre'] = datosQuery1[0]['cat_nombre']
        datos['descripcion'] = datosQuery1[0]['cat_descripcion']
        datos['result'] = "OK"
    except Exception as exc:
        print(exc)
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_categoria(request):
    datos = {}
    id_categ = request.GET.get("id")
    registro=categoria()
    try:
        registro.remove_categoria(id_categ)

        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_categorias_select(request):
    datos = []
    return HttpResponse(json.dumps(datos), content_type="application/json")

# Marcas ***************************************************************************************************************
def listar_marcas(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    registro=marca()
    datosQuery=[]
    datosQuery=registro.view_marca()
    dic = {
        'marcas': datosQuery
    }
    return render(request, "dashboard/ajustes/marcas.html", dic)

def agregar_marca(request):
    datos = {}
    nombre = request.POST.get('marca_producto')
    id_mar = request.POST.get('marca_id')
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")
    registro=marca()
    datosQuery1=[]
    datosQuery=[]
    try:
        if not id_mar:
            try:
                datosQuery=registro.new_marca(nombre)
            except Exception as e:
                print(e)
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")
        else:
            datosQuery=registro.update_marca(id_mar,nombre)
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        datosQuery1=registro.view_marca_nombre(nombre)
        mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        datos['message'] = mensaje
        datos['id'] = datosQuery1[0][0]
        datos['nombre'] = datosQuery1[0][1]
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_marca(request):
    datos = {}
    id_mar = request.GET.get("id")
    datosQuery=[]
    registro=marca()
    try:
        datosQuery=registro.view_marca_id(id_mar)
        datos['id'] = datosQuery[0]['mar_id']
        datos['nombre'] = datosQuery[0]['mar_nombre']
        datos['result'] = "OK"
    except Exception as exc:
        print(exc)
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_marca(request):
    datos = {}
    id_mar = request.GET.get("id")
    registro=marca()
    try:
        registro.remove_marca(id_mar)
        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Clientes *************************************************************************************************************
def form_agregar_cliente(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    dic = {}
    return render(request, "dashboard/cliente/agregar.html", dic)

def form_editar_cliente(request, id_cli):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    registro=User()
    if id_cli:
        cliente_editar = registro.view_user_id(id_cli)
        print(cliente_editar)
        if len(cliente_editar)>0:
            print(cliente_editar)
        else:
            return HttpResponseRedirect(reverse("dashboard_app:url_listar_clientes"))

    dic = {
        'cliente_editar': cliente_editar
    }
    return render(request, "dashboard/cliente/agregar.html", dic)

def agregar_cliente(request):
    datos = {}
    usuario = request.POST.get('cliente_usuario')
    contrasena = request.POST.get('cliente_contrasena')
    nombre = request.POST.get('cliente_nombre')
    apellido = request.POST.get('cliente_apellido')
    email = request.POST.get('cliente_email')
    telefono = request.POST.get('cliente_telefono')
    avatar = request.FILES.get('id_avatar_usuario')
    id_cliente = request.POST.get('id_cliente')
    registro=User()
    datosQuery1=[]
    datosQuery=[]
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")
    bandera = "A"
    try:
        print(id_cliente)
        if not id_cliente:
            print('no debiop entrar')
            try:
                datosQuery=registro.new_user(usuario, make_password(contrasena) , nombre, apellido, telefono, email, avatar)
            except Exception as e:
                print(e)
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")
        else:
            datosQuery=[]
            datosQuery=registro.update_user(id_cliente, usuario, nombre, apellido, telefono, email, avatar)
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")
            bandera = "E"
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")
        datos['message'] = mensaje
        datos['bandera'] = bandera
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def listar_clientes(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    datosQuery=[]
    registro=User()
    datosQuery=registro.view_user()
    dic = {
        'clientes': datosQuery
    }
    return render(request, "dashboard/cliente/administrar.html", dic)

def eliminar_clientes(request):
    datos = {}
    id_cli = request.GET.get("id")
    registro=User()
    try:
        registro.update_user_inactivo(id_cli,0)
        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def superusuario_cliente(request):
    datos = {}
    id_cli = request.GET.get("id")
    estado = request.GET.get("estado")
    registro=User()
    try:
        if estado == "true":
            registro.update_user_superuser(id_cli,1)
        else:
            registro.update_user_superuser(id_cli,0)
        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def activar_clientes(request):
    datos = {}
    id_cli = request.GET.get("id")
    registro=User()
    try:
        registro.update_user_inactivo(id_cli,1)
        datos['result'] = "OK"
        datos['id'] = id_cli
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cambio_clave_cliente(request):
    datos = {}
    id_cli = request.POST.get("id_cliente")
    clave_actual = request.POST.get("cli_clave_actual")
    clave_nueva1 = request.POST.get("cli_clave_nueva1")
    clave_nueva2 = request.POST.get("cli_clave_nueva2")
    registro=User()
    datosQuery=[]
    datosQuery=registro.view_user_id(id_cli)
    try:
        logueo = authenticate(username=datosQuery[0]['username'], password=clave_actual)

        if clave_nueva1 != clave_nueva2:
            datos['message'] = gettext("Las contraseñas ingresadas no coinciden.").decode("utf-8")
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        if logueo:
            registro.update_user_password(id_cli,make_password(clave_nueva1))
        else:
            datos['message'] = gettext("La contraseña actual no es la correcta.").decode("utf-8")
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        datos['result'] = "OK"
        datos['message'] = gettext("La contraseña ha sido cambiada correctamente.").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Pedidos **************************************************************************************************** *********
def listar_pedidos(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    dic = {
        'pedidos': pedido().view_pedido()
    }
    return render(request, "dashboard/pedidos/administrar.html", dic)

def ver_detalle_pedido(request, id_ped):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    if not  len(pedido().view_pedido_id(id_ped))>0:
        return HttpResponseRedirect(reverse("dashboard_app:url_listar_pedidos"))

    dic = {
        'detalle': pedido().view_pedido_id(id_ped),
        'items': det_pedido().view_detalle_pedido_id(id_ped),
        'ruta': request.META.get('HTTP_REFERER')
    }
    return render(request, "dashboard/pedidos/detalle_pedido.html", dic)

def cambiar_estado_pedidos(request):
    datos = {}
    id_ped = request.POST.get("id_pedido")
    fecha = request.POST.get("id_pedido_fecha")
    estado = request.POST.get("id_estado_pedido")
    guia = request.POST.get("id_gui_pedido")

    try:
        lista = ["pendiente", "enviado", "verificacion", "cancelado"]
        if not estado in lista:
            datos['message'] = gettext("El valor del estado no es el correcto. Actualice la página e inténtelo de nuevo.").decode("utf-8")
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        obj_ped=pedido().view_pedido_id(id_ped)
        fecha_envio = datetime.datetime.strptime(fecha, "%Y/%m/%d").strftime("%Y-%m-%d %H:%M")
        pedido().update_pedido(id_ped, str(fecha_envio), estado, guia)

        if obj_ped[0]['ped_estado'] == estado:
            datos['message'] = gettext("El estado debe ser diferente al que posee actualmente.").decode("utf-8")
            datos['result'] = "X"

            return HttpResponse(json.dumps(datos), content_type="application/json")

        datos['id'] = id_ped
        datos['cliente'] = obj_ped[0]['cliente_id']
        datos['estado'] = estado
        datos['result'] = "OK"
        datos['message'] = gettext("El estado del pedido ha cambiado a").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido cambiar el estado del pedido. COD.: " + str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_deposito(request):
    datos = {}
    id_ped = request.GET.get("id")

    try:
        ped = pedido().view_pedido_id(id_ped)
        datos['imagen'] = '/media/'+ped[0]['ped_imagen_deposito']
        datos['codigo'] = ped[0]['ped_cod_deposito']
        datos['id_p'] = id_ped
        datos['fecha'] = str(ped[0]['ped_fecha_envio']).replace("-", "/")
        datos['estado'] = ped[0]['ped_estado']
        datos['guia'] = ped[0]['ped_codigo_guia']
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_ped_cliente(request):
    datos = {}
    id_ped = request.GET.get("id")

    try:
        ped=pedido().view_pedido_id(id_ped)
        cli = User().view_user_id(ped[0]['cliente_id'])
        datos['imagen'] = '/static/proyecto/img/sin-imagen-user.png'
        datos['usuario'] = cli[0]['username']
        datos['nombre'] = cli[0]['us_nombre']+" "+ cli[0]['us_apellidos']
        datos['telefono'] = cli[0]['us_telefono_movil']
        datos['email'] = cli[0]['email']
        datos['fecha'] = str(ped[0]['ped_fecha_envio']).replace("-", "/")
        datos['direccion'] = ped[0]['ped_direccion_envio']
        datos['ciudad'] = ped[0]['ped_ciudad_envio']+" - "+ped[0]['ped_pais_envio']
        datos['cod_postal'] = ped[0]['ped_cod_postal_envio']
        datos['result'] = "OK"
    except Exception as exc:
        print(exc)
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_pedido(request):
    datos = {}
    id_ped = request.GET.get("id")

    try:
        pedido().remove_pedido(id_ped)

        datos['result'] = "OK"
        datos['message'] = gettext("El pedido ha sido eliminado correctamente.").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

# Transporte ***********************************************************************************************************
def listar_transporte(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    registro=emp_trasporte()
    datosQuery=[]
    datosQuery=registro.view_transporte()
    dic = {
        'transporte': datosQuery
    }
    return render(request, "dashboard/ajustes/transporte.html", dic)

def agregar_transporte(request):
    datos = {}
    nombre = request.POST.get('ajustes_transporte_nombre')
    costo = request.POST.get('ajustes_transporte_costo')
    direccion_url = request.POST.get('ajustes_transporte_url')
    id_trans = request.POST.get('transporte_id')
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")
    registro=emp_trasporte()
    try:
        if not id_trans:
            try:
                datosQuery1=[]
                datosQuery=[]
                datosQuery=registro.new_transporte(nombre, costo, direccion_url)
            except Exception as e:
                datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")
        else:
                datosQuery=[]
                datosQuery=registro.update_transporte(id_trans,nombre, costo, direccion_url)
                mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")

        datosQuery1=registro.view_transporte_nombre(nombre)
        print(datosQuery1)
        datos['message'] = mensaje
        datos['id'] = datosQuery1[0][0]
        datos['nombre'] = datosQuery1[0][1]
        datos['costo'] = str(datosQuery1[0][2])
        datos['url'] = datosQuery1[0][3]
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_transporte(request):
    datos = {}
    datosQuery1=[]
    id_trans = request.GET.get("id")
    registro=emp_trasporte()
    try:
        datosQuery1=registro.view_transporte_id(id_trans)
        datos['id'] = datosQuery1[0]['trans_id']
        datos['nombre'] = datosQuery1[0]['trans_nombre']
        datos['costo'] = str(datosQuery1[0]['trans_costo']).replace(",", ".")
        datos['url'] = datosQuery1[0]['trans_url']
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_transporte(request):
    datos = {}
    id_trans = request.GET.get("id")
    registro=emp_trasporte()
    try:
        registro.remove_transporte(id_trans)
        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

#  Impuestos ***********************************************************************************************************
def listar_impuestos(request):
    registro=impuesto()
    datosQuery=[]
    for i in registro.view_impuesto():
        datosQuery.append({'imp_id':i[0], 'imp_nombre': i[1], 'imp_porcentaje':i[2], 'imp_actual':i[3]})
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))
    dic = {
        'impuesto': datosQuery
    }
    return render(request, "dashboard/ajustes/impuestos.html", dic)

def agregar_impuestos(request):
    datos = {}
    nombre=request.POST.get('ajustes_impuesto_nombre')
    porcentaje=request.POST.get('ajustes_impuesto_porcentaje')
    id_imp = request.POST.get('ajustes_id_impuesto')
    mensaje = gettext("El registro ha sido creado correctamente").decode("utf-8")
    try:
        registro=impuesto()
        datosQuery=[]
        datosQuery1=[]
        for i in registro.view_impuesto():
            datosQuery.append([i[0],i[1],i[2]])
        if not id_imp:
            for l in datosQuery:
                if (str(l[1]) == nombre):
                    datos['message'] = gettext("No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre").decode("utf-8")
                    datos['result'] = "X"
                    return HttpResponse(json.dumps(datos), content_type="application/json")
            if len(datosQuery) > 0:
                print(registro.new_impuesto(nombre, porcentaje, 0), ' Se agrego el registro')
            else:
                print(registro.new_impuesto(nombre, porcentaje, 1), ' Se agrego el registro')
            for i in registro.view_impuesto_nombre(0, nombre):
                datosQuery1.append([i[0],i[1],i[2]])
        else:
            for i in registro.view_impuesto_nombre(0, nombre):
                datosQuery1.append([i[0],i[1],i[2], i[3]])
            print(registro.update_impuesto(id_imp, nombre, porcentaje), ' Se actualizo el registro')
            mensaje = gettext("El registro ha sido actualizado correctamente").decode("utf-8")


        print(datosQuery1)

        datos['message'] = mensaje
        datos['id'] = datosQuery1[0][0]
        datos['actual'] = datosQuery1[0][2]
        datos['result'] = "OK"
    except Exception as exc:
        print(exc)
        datos['message'] = gettext("Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_impuestos(request):
    datos = {}
    datosQuery1=[]
    id_impuesto = request.GET.get("id")
    registro=impuesto()
    try:
        for i in registro.view_impuesto_id(id_impuesto, 0):
            datosQuery1.append([i[0],i[1],i[2]])
        datos['id'] = datosQuery1[0][0]
        datos['nombre'] = datosQuery1[0][1]
        datos['porcentaje'] = datosQuery1[0][2]
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("El registro no existe. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminar_impuestos(request):
    datos = {}
    datosQuery1=[]
    id_impuesto = request.GET.get("id")
    registro=impuesto()
    for i in registro.view_impuesto_id(id_impuesto, 0):
        datosQuery1.append([i[0],i[1],i[2], i[3]])
    try:
        if datosQuery1[0][3]==1:
            datos['message'] = gettext("No se ha podido eliminar el registro. Es el impuesto usado actualmente").decode("utf-8")
            datos['result'] = "X"
            return HttpResponse(json.dumps(datos), content_type="application/json")
        registro.remove_impuesto(id_impuesto)
        datos['result'] = "OK"
        datos['message'] = gettext("El registro ha sido eliminado correctamente").decode("utf-8")
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def impuesto_actual(request):
    datos = {}
    id_imp = request.GET.get("id")
    try:
        registro=impuesto()
        print(registro.update_impuesto_campo(0, 0))
        print(registro.update_impuesto_campo(1, id_imp))
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = gettext("No se ha podido eliminar el registro. COD.: "+str(exc.message)).decode("utf-8")
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")


# Reviews **************************************************************************************************************
def listar_reviews(request):
    dic = {
    }
    return render(request, "dashboard/productos/reviews.html", dic)

def eliminar_reviews(request):
    datos = {}
    return HttpResponse(json.dumps(datos), content_type="application/json")

# Imagenes *********************************************************************************************** *************
def listar_imagenes(request):
    dic = {
    }
    return render(request, "dashboard/productos/imagenes.html", dic)

def eliminar_imagenes(request):
    datos = {}
    return HttpResponse(json.dumps(datos), content_type="application/json")

@transaction.atomic()
def cambiar_imagenes(request):
    datos = {}
    return HttpResponse(json.dumps(datos), content_type="application/json")

@transaction.atomic()
def subir_nueva_imagen(request):
    datos = {}
    return HttpResponse(json.dumps(datos), content_type="application/json")


# Funciones globales ***************************************************************************************************
def verificar_stock_minimo():
    productos_bajo_stock = 0
    return  productos_bajo_stock

# Verificar pedidos pendientes
def verificar_pedidos_pendientes():
    pedidos_pendientes = pedido().view_pedido_filter()
    return  pedidos_pendientes

#Cargar todos los tags
def cargar_todos_tags(request):
    datos = []
    tags = tag().view_tag()
    for t in tags:
        datos.append({'value': t['tag_id'], 'label': t['tag_nombre']})

    return HttpResponse(json.dumps(datos), content_type="application/json")

#Eliminar archivos de la carpeta media (fisica)
def borrar_archivos_media(ruta):
    estado = True
    try:
        os.remove(ruta)
    except:
        estado = False

    return estado

#Filtrar nombre de producto en el buscador del datatable
def filtrar_nombre_producto(request):
    datos = {}
    return HttpResponse(json.dumps(datos), content_type="application/json")

def listar_pedidos_pendientes(request):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse("users_app:login"))

    pedidos_pendientes = verificar_pedidos_pendientes()
    dic = {
        'notif_pedidos': pedidos_pendientes
    }
    return render(request, "dashboard/ajustes/notif_pedidos.html", dic)

#Devuelve todos los productos (id-nombre)
def json_productos(request):
    datos = []
    prod = producto().view_producto()

    for p in prod:
        datos.append({'id': p['pro_id'], 'text': p['pro_nombre']})

    return HttpResponse(json.dumps(datos), content_type="application/json")

