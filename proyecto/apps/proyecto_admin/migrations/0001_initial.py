# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='impuesto',
            fields=[
                ('imp_id', models.AutoField(serialize=False, primary_key=True)),
                ('imp_nombre', models.CharField(max_length=100)),
                ('imp_porcentaje', models.FloatField(default=0)),
                ('imp_actual', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name_plural': 'Impuestos',
            },
        ),
        migrations.CreateModel(
            name='perfil_empresa',
            fields=[
                ('per_id', models.AutoField(serialize=False, primary_key=True)),
                ('per_nombre', models.CharField(max_length=100)),
                ('per_email', models.CharField(max_length=100)),
                ('per_direccion', models.CharField(max_length=250)),
                ('per_telefono', models.CharField(max_length=30)),
                ('per_celular', models.CharField(max_length=30)),
                ('per_logo', models.ImageField(upload_to=b'logos_empresas')),
                ('per_ruc', models.CharField(max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Perfiles',
            },
        ),
    ]
