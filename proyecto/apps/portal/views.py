from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect, HttpResponseRedirect
from django.contrib import messages
from django.core.mail import send_mail


# Create your views here.
def inicio(request):
	context={}
	return render_to_response('index.html', context_instance=RequestContext(request, context))