# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0002_galeria'),
    ]

    operations = [
        migrations.CreateModel(
            name='redesSociales',
            fields=[
                ('red_id', models.AutoField(serialize=False, primary_key=True)),
                ('red_usuario', models.CharField(max_length=40, verbose_name=b'Usuario', blank=True)),
                ('red_link', models.CharField(max_length=40, verbose_name=b'Link', blank=True)),
                ('red_imagen', models.ImageField(upload_to=b'portal/redes', null=True, verbose_name=b'Imagen', blank=True)),
            ],
        ),
    ]
