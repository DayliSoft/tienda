# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='galeria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gal_etiqueta', models.CharField(max_length=40, verbose_name=b'Etiqueta', blank=True)),
                ('gal_imagen', models.ImageField(upload_to=b'portal/galeria', null=True, verbose_name=b'Imagen', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Galeria del Portal',
            },
        ),
    ]
