# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='portal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('por_etiqueta', models.CharField(max_length=40, verbose_name=b'Etiqueta', blank=True)),
                ('por_texto', models.CharField(max_length=500, verbose_name=b'Texto', blank=True)),
                ('por_imagen', models.ImageField(upload_to=b'portal', null=True, verbose_name=b'Imagen', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Textos para el Portal',
            },
        ),
    ]
