# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0003_redessociales'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='redessociales',
            options={'verbose_name_plural': 'Galeria del Portal'},
        ),
        migrations.AddField(
            model_name='redessociales',
            name='red_nombre',
            field=models.CharField(max_length=50, verbose_name=b'Nombre', blank=True),
        ),
    ]
