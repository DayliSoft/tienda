# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0007_auto_20161109_1237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portal',
            name='por_texto',
            field=models.CharField(max_length=2000, verbose_name=b'Texto', blank=True),
        ),
    ]
