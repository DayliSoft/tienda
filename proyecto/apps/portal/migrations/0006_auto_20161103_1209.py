# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0005_auto_20161103_1209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='redessociales',
            name='red_link',
            field=models.CharField(max_length=300, verbose_name=b'Link', blank=True),
        ),
    ]
