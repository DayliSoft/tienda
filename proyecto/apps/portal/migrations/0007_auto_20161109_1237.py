# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0006_auto_20161103_1209'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='redessociales',
            options={'verbose_name_plural': 'Redes sociales'},
        ),
    ]
