# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0004_auto_20161103_1152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='redessociales',
            name='red_usuario',
            field=models.CharField(max_length=200, verbose_name=b'Usuario', blank=True),
        ),
    ]
