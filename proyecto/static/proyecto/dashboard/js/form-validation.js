//==========================================================
//  Validacion provedor
//==========================================================
$().ready(function() {
    $("#formulario_agregar_proveedor").validate({
        rules: {
            proveedor_nombre: {
                required: true,
                maxlength: 50
            },
            proveedor_contacto: {
                required: true,
                maxlength: 50
            },
            proveedor_direccion: {
                required: true,
                maxlength: 50
            },
            proveedor_ciudad: {
                required: true,
                maxlength: 20
            },
            proveedor_pais: {
                required: true,
                maxlength: 10
            },
            proveedor_telefono: {
                required: true,
                maxlength: 10
            },
            proveedor_celular: {
                required: true,
                maxlength: 10
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});



//==========================================================
//  Validacion de productos-general
//==========================================================
$().ready(function() {
    $("#form_producto_general").validate({
        ignore: [],
        rules: {
            nombre_producto: {
                required: true,
                maxlength: 50
            },
            descripcion_producto_corta: {
                required: true,
                maxlength: 360
            },
            categoria_producto: {
                required: true
            },
            id_marca_producto: {
                required: true
            },
            informacion_ad_producto: {
                maxlength: 200
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    })
});

//==========================================================
//  Validacion de productos-inventario
//==========================================================
$().ready(function() {
    $("#form_producto_inventario").validate({
        ignore: [],
        rules: {
            precio_compra_producto: {
                required: true,
                number:true,
                min: 1
            },
            precio_venta_producto: {
                required: true,
                number:true,
                greaterThanSelling: "#precio_compra_producto",
                min: 1
            },
            stock_producto: {
                required: true,
                number:true,
                min: 1
            },
            stock_min_producto: {
                required: true,
                number:true,
                min: 1
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    })
});

//==========================================================
//  Validacion de productos-oferta
//==========================================================
$().ready(function() {
    $("#form_producto_oferta").validate({
        ignore: [],
        rules: {
            producto_precio_oferta: {
                number:true,
                required: true,
                min: 0.1
            },
            producto_fecha_inicio: {
                required: true
            },
            producto_fecha_fin: {
                required: true,
                greaterThanDate: "#producto_fecha_inicio"
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    })
});

//==========================================================
//  Validacion de prod. en cuarentena
//==========================================================
$().ready(function() {
    $("#formulario_producto_cuarentena").validate({
        rules: {
            id_producto_cuarentena: {
                required: true
            },
            cuarentena_cantidad: {
                required: true,
                number: true,
                min: 1
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion de categorias
//==========================================================
$().ready(function() {
    $("#formulario_producto_categoria").validate({
        rules: {
            id_categoria_hijo: {
                required: true,
                maxlength: 30
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion de marcas
//==========================================================
$().ready(function() {
    $("#formulario_producto_marca").validate({
        rules: {
            marca_producto: {
                required: true,
                maxlength: 50
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion de transporte
//==========================================================
$().ready(function() {
    $("#formulario_ajustes_transporte").validate({
        rules: {
            ajustes_transporte_nombre: {
                required: true,
                maxlength: 50
            },
            ajustes_transporte_costo: {
                required: true,
                number: true,
                min: 0.1
            },
            ajustes_transporte_url: {
                required: true,
                maxlength: 50
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion de impuesto
//==========================================================
$().ready(function() {
    $("#formulario_ajustes_impuesto").validate({
        rules: {
            ajustes_impuesto_nombre: {
                required: true,
                maxlength: 100
            },
            ajustes_impuesto_porcentaje: {
                required: true,
                number: true,
                min: 0.1
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion perfil de empresa
//==========================================================
$().ready(function() {
    $("#formulario_ajustes_empresa").validate({
        rules: {
            ajustes_empresa_nombre: {
                required: true,
                maxlength: 100
            },
            ajustes_empresa_email: {
                required: true,
                email: true,
                maxlength: 100
            },
            ajustes_empresa_direccion: {
                required: true,
                maxlength: 250
            },
            ajustes_empresa_telefono: {
                required: true,
                maxlength: 30
            },
            ajustes_empresa_ruc: {
                required: true,
                maxlength: 20
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion campania email
//==========================================================
$().ready(function() {
    $("#formulario_camapania_email").validate({
        rules: {
            email_campania_nombre: {
                required: true,
                maxlength: 100
            },
            email_campania_asunto: {
                required: true,
                maxlength: 100
            },
            email_campania_cuerpo: {
                required: true
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion de clientes
//==========================================================
$().ready(function() {
    $("#formulario_agregar_cliente").validate({
        rules: {
            cliente_nombre: {
                required: true,
                maxlength: 100
            },
            cliente_apellido: {
                required: true,
                maxlength: 100
            },
            cliente_email: {
                email:true,
                maxlength: 254
            },
            cliente_telefono: {
                required: true,
                maxlength: 10
            },
            cliente_contrasena: {
                required: true,
                maxlength: 10,
                minlength: 5
            },
            cliente_usuario: {
                required: true,
                maxlength: 100
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion de cambio de clave cliente
//==========================================================
$().ready(function() {
    $("#formulario_cambio_clave").validate({
        rules: {
            cli_clave_actual: {
                required: true,
                minlength: 5,
                maxlength: 10
            },
            cli_clave_nueva1: {
                required: true,
                minlength: 5,
                maxlength: 10
            },
            cli_clave_nueva2: {
                required: true,
                minlength: 5,
                maxlength: 10,
                equalTo: "#cli_clave_nueva1"
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion de Ofertas crud
//==========================================================
$().ready(function() {
    $("#formulario_crud_ofertas").validate({
        ignore: [],
        rules: {
            producto_precio_oferta_crud: {
                number:true,
                required: true,
                min: 0.1
            },
            producto_fecha_inicio_crud: {
                required: true
            },
            producto_fecha_fin_crud: {
                required: true,
                greaterThanDate: "#producto_fecha_inicio_crud"
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    })
});

//==========================================================
//  Validacion de estado de pedido
//==========================================================
$().ready(function() {
    $("#formulario_estado_pedido").validate({
        ignore: [],
        rules: {
            id_gui_pedido: {
                maxlength: 50
            },
            id_pedido_fecha: {
                required: true
            }
        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    })
});