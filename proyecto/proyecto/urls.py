"""daylisoft_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

#Internacionalizacion para javascript
js_info_dict = {
    'domain': 'djangojs',
    'packages': ('wrnpro', ),
}

urlpatterns =patterns ('',
    url(r'^user/', include('apps.users.urls', namespace="users_app")),
    url(r'^', include('apps.portal.urls', namespace="portal_app")),
    url(r'^shop/', include('apps.shop.urls', namespace="shop_app")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^dashboard/', include('apps.proyecto_admin.urls', namespace="dashboard_app")),

    # Internacionalizacion
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^jsi18n/$', 'django.views.i18n.javascript_catalog', js_info_dict),
    url(r'^rosetta/', include('rosetta.urls')),
)
if settings.DEBUG:
    urlpatterns += patterns("",
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT,}
        ),
)
