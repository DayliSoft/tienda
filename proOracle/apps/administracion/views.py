from django.shortcuts import render_to_response, RequestContext, redirect, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User, Group, Permission
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from apps.portal.models import *
from django.contrib import messages

def inicioSesion(request):
    if request.user.is_authenticated():
        return redirect('/')
    else:
        if request.POST:
            username =request.POST['txtUsuario']
            password =request.POST['txtPassword']
            user=authenticate(username=username, password=password)
            if user is not None and user.is_active:
                login(request, user)
                return redirect('/')
            else:
                messages.add_message(request, messages.ERROR, 'Nombre de usuario y Password incorrecto'.upper())
                return redirect('/')

@login_required(login_url='/')
def cerrarSesion(request):
    logout(request)
    return redirect('/')


@login_required(login_url='/')
def perfil(request):
    dic={
        'tittle':'Bienvenido a SPA'
    }
    return render_to_response('users/perfil.html', dic,context_instance=RequestContext(request))

@login_required(login_url='/')
def guardarPerfil(request):
    usuario = request.POST.get('username')
    nombre = request.POST.get('nombre')
    apellido = request.POST.get('apellido')
    email = request.POST.get('email')
    try:
        usuario = User.objects.get(id=request.user.id)
        usuario.first_name = nombre
        usuario.last_name = apellido
        usuario.email = email
        usuario.save()
        messages.add_message(request, messages.SUCCESS, 'Se modifico su perfil')

    except Exception as e:
        messages.add_message(request, messages.ERROR, e.message)

    return redirect('/users/perfil/')