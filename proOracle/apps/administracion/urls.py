from django.conf.urls import patterns, include, url
from .views import *
from apps.administracion.views import *
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
     url(r'^login/$', inicioSesion),
     url(r'^cerrarsesion/$', cerrarSesion),
     url(r'^perfil/$', perfil, name="perfil"),
     url(r'^guardarPerfil/$', guardarPerfil, name="guardarPerfil"),
     )
