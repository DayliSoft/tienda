from django.contrib import admin
from apps.portal.models import *
# Register your models here.
admin.site.register(Categoria)
admin.site.register(Producto)
admin.site.register(Factura)
admin.site.register(Detalle_Factura)
admin.site.register(Cliente)
admin.site.register(emp_trasporte)
