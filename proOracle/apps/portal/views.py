from unicodedata import decimal
from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect
from .models import *
from decimal import Decimal
from django.core import serializers
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from apps.portal.models import *
from apps.portal.views import *
from django.utils import simplejson
from django.contrib.auth.decorators import login_required
from apps.portal.conexionesOracle import *

# Create your views here.
def index(request):
    dic={
        'tittle':'Bienvenido a SPA',
        'titulo':'SPA',
        'categoria':categoriasQuery(),
        'ultimosProductos':productosUltimosQuery()
    }
    return render_to_response('index.html', dic,context_instance=RequestContext(request))

def categoriaBusq(request, parametro):
    dic={
        'tittle': categoriasQueryById(parametro),
        'titulo':categoriasQueryById(parametro),
        'categoria':categoriasQuery(),
        'ultimosProductos':productosByCategoria(parametro),
    }
    return render_to_response('index.html', dic,context_instance=RequestContext(request))

#PRODUCTOS
@login_required(login_url='/')
def productos(request):
    dict={}
    dict['tittle']='LISTAR PRODUCTOS'
    dict['categoria']=categoriasQuery()
    dict['listaProductos']=productosAll()
    if (request.user.is_superuser and request.user.is_staff):
        dict['usuario'] = request.user
        return render_to_response('productos/productos.html', dict,context_instance=RequestContext(request))
    else:
        dict['usuario'] = request.user
        return redirect('/')

import json
def activarProducto(request):
    print('entro')
    try:
        variable = request.GET['codigo']
        estado = request.GET['estado']
        v=productosById(variable)
        mensaje ="El producto ha quedado inactivo"
        if int(estado) == 0:
            v.is_active = False
        else:
            v.is_active = True
            mensaje = "El producto ha sido activado nuevamente"
        v.save()
        dic = {
            'mensaje':mensaje,
            'result': 'OK'
        }

    except Exception as e:
        print(e)
        dic = {
                'mensaje':"Error al eliminar/activar el usuario",
                'result': 'X'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

def modificarProducto(request):
    variable = request.GET['codigo']
    data = []
    try:
        p = productosById(variable)
        ca=categoriasByFilter(p.id_categoria_id)
        data.append({'codigo': p.codigo,
                     'id': p.id,
                     'nombre': p.nombre,
                     'descripcion_corta': str(p.descripcion_corta),
                     'descripcion_larga':str(''), 'stock': str(p.stock),
                     'precio_compra': str(p.precio_compra),
                     'precio_venta': str(p.precio_venta),
                     'iva': p.iva,
                     'descuento':str(p.descuento), 'marca': '',
                     'modelo':p.modelo,
                        'observacion':''})
        return HttpResponse(json.dumps(data), content_type="application/json")

    except Exception as error:
        print(error)
        data = []
    return HttpResponse(data, mimetype='application/json')



def modificarProductoGuardar(request):
    id_pro=request.GET["txt_producto_id"]
    codigo=request.GET["txt_producto_codigo"]
    nombre=request.GET["txt_producto_nombre"]
    dcorta=request.GET["txt_producto_descripcion_corta"]
    stock=request.GET["txt_producto_stock"]
    precioc=request.GET["txt_producto_precio_compra"]
    preciov=request.GET["txt_producto_precio_venta"]
    iva=request.GET["txt_producto_iva"]
    descuento=request.GET["txt_producto_descuento"]
    modelo=request.GET["txt_producto_modelo"]
    categoria = request.GET.get('txt_producto_categoria')
    try:
        imagen=request.FILES["imagen"]
        dic=guardarProductoById(id_pro, codigo, nombre, dcorta, stock, precioc, preciov, iva, descuento, modelo, categoria, imagen, 0)
    except Exception as e:
        dic=guardarProductoById(id_pro, codigo, nombre, dcorta, stock, precioc, preciov, iva, descuento, modelo, categoria, '', 1)
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")


def guardarProducto(request):
    if request.POST:
        codigo=request.POST["txt_producto_codigo"]
        nombre=request.POST["txt_producto_nombre"]
        dcorta=request.POST["txt_producto_descripcion_corta"]
        stock=request.POST["txt_producto_stock"]
        precioc=request.POST["txt_producto_precio_compra"]
        preciov=request.POST["txt_producto_precio_venta"]
        iva=request.POST["txt_producto_iva"]
        descuento=request.POST["txt_producto_descuento"]
        try:
            imagen=request.FILES["imagen"]
        except:
            messages.add_message(request, messages.ERROR, "Ninguna Imagen Seleccionada")
            return redirect('/productos/')

        modelo=request.POST["txt_producto_modelo"]
        categoria=request.POST.getlist('listCategorias')
        try:
            saber=False

            productos = productosByFilter(codigo)
            for r in productos:
                if r.codigo == codigo:
                    saber=True
            if(saber):
                # suma=int(stockac)+int(stock)
                dic=guardarProductoByCodigo(codigo, nombre, dcorta, stock, precioc, preciov, iva, descuento, modelo, categoria)
                messages.add_message(request, messages.SUCCESS, "Se actualizo el producto")
            else:
                iva1=False
                if(iva=='si'):
                    iva1=True
                guardar= guardarProductoQuery(codigo, nombre, dcorta, stock, precioc, preciov, iva1, descuento, modelo, categoria, imagen)
                messages.add_message(request, messages.SUCCESS, "Producto Guardado")
            return redirect('/productos/')
        except Exception as e:
            print(e)
            messages.add_message(request, messages.ERROR, "Error al guardar Producto")
            return redirect('/productos/')
    return redirect('/productos/')



#CATEGORIAS
@login_required(login_url='/')
def categorias(request):
    dict={}
    dict['tittle']='LISTAR PRODUCTOS'
    dict['listaCategoria']=categoriasQuery()
    dict['listaProductos']=productosAll()
    if (request.user.is_superuser and request.user.is_staff):
        dict['usuario'] = request.user
        return render_to_response('productos/categorias.html', dict,context_instance=RequestContext(request))
    else:
        dict['usuario'] = request.user
        return redirect('/')

def guardarCategoria(request):
    if request.POST:
        nombre=request.POST["txt_categoria_nombre"]
        descripcion=request.POST["txt_categoria_descripcion"]
        guardarCategoriaQuery(nombre, descripcion)
        messages.add_message(request, messages.SUCCESS, "Categoria Guardada")
    return redirect('/categorias/')


def modificarCategoria(request):
    variable = request.GET['codigo']
    data = []
    ca=categoriasQueryByIdAll(variable)
    data.append({
                 'id': ca.id,
                 'nombre': ca.nombre,
                 'descripcion': str(ca.descripcion)
                 })
    return HttpResponse(json.dumps(data), content_type="application/json")

def modificarCategoriaGuardar(request):
    id_pro=request.GET["txt_categoria_id"]
    nombre=request.GET["txt_categoria_nombre"]
    dcorta=request.GET["txt_categoria_descripcion"]
    modificarCategoriaById(id_pro, nombre, dcorta)
    dic = {
        'mensaje': "Se actualizo correctamente"
    }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")


#CLIENTES
@login_required(login_url='/')
def clientes(request):
    dict={}
    dict['tittle']='LISTAR CLIENTES'
    dict['listaClientes']=clientesAll()
    if (request.user.is_superuser and request.user.is_staff):
        dict['usuario'] = request.user
        return render_to_response('clientes/clientes.html', dict,context_instance=RequestContext(request))
    else:
        dict['usuario'] = request.user
        return redirect('/')

def guardarCliente(request):
    if request.POST:
        ruc=request.POST["txt_cliente_ruc"]
        nombre=request.POST["txt_cliente_nombre"]
        apellido=request.POST["txt_cliente_apellido"]
        email=request.POST["txt_cliente_email"]
        try:
            guardar= guardarClienteQuery(ruc, nombre, apellido, email)
            messages.add_message(request, messages.SUCCESS, "Cliente Guardado")
            return redirect('/clientes/')
        except Exception as e:
            print(e)
            messages.add_message(request, messages.ERROR, "Error al guardar Cliente")
            return redirect('/clientes/')
    return redirect('/clientes/')

def modificarCliente(request):
    variable = request.GET['codigo']
    data = []
    p = ClientesById(variable)
    data.append({'id': p.id,
                 'nombre': p.nombre,
                 'ruc': p.ruc_cedula,
                 'apellido': str(p.apellido),
                 'email':p.correo})
    return HttpResponse(json.dumps(data), content_type="application/json")

def modificarClienteGuardar(request):
    id_cli=request.GET["id"]
    ruc=request.GET["txt_cliente_ruc"]
    nombre=request.GET["txt_cliente_nombre"]
    apellido=request.GET["txt_cliente_apellido"]
    email=request.GET["txt_cliente_email"]
    dic=guardarClienteById(id_cli, ruc, nombre, apellido, email)
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")


import json
def eliminarCliente(request):
    try:
        variable = request.GET['codigo']
        v=ClientesById(variable)
        mensaje ="Se elimino el cliente"
        v.delete()
        dic = {
            'mensaje':mensaje,
            'result': 'OK'
        }

    except Exception as e:
        print(e)
        dic = {
                'mensaje':"Error al eliminar/activar el usuario",
                'result': 'X'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")