from django.db import models

class Cliente(models.Model):
    id=models.AutoField(primary_key=True, unique=True)
    ruc_cedula=models.CharField(max_length=20)
    nombre=models.CharField(max_length=100)
    apellido=models.CharField(max_length=100)
    correo=models.EmailField()


class Factura(models.Model):
    fecha=models.DateField(auto_now_add=True)
    subtotal=models.DecimalField(max_digits=8, decimal_places=2)
    iva=models.DecimalField(max_digits=8, decimal_places=2)
    total=models.DecimalField(max_digits=8, decimal_places=2)
    cliente_id=models.ForeignKey(Cliente)

class Categoria(models.Model):
    id=models.AutoField(primary_key=True, unique=True)
    nombre=models.CharField(max_length=100)
    descripcion=models.CharField(max_length=100, blank=True, null=True)
    def __unicode__(self):
        return self.nombre

class Producto(models.Model):
    id=models.AutoField(primary_key=True, unique=True)
    codigo=models.CharField(max_length=50)
    nombre=models.CharField(max_length=200)
    descripcion_corta=models.CharField(max_length=200)
    descripcion_larga=models.TextField()
    stock=models.PositiveIntegerField()
    precio_compra=models.DecimalField(max_digits=8, decimal_places=2)
    precio_venta=models.DecimalField(max_digits=8, decimal_places=2)
    iva=models.BooleanField()
    descuento=models.DecimalField(max_digits=4,decimal_places=2)
    is_active=models.BooleanField()
    fecha_ingreso=models.DateField(auto_now_add=True)
    imagen = models.ImageField(upload_to='producto/%Y/%m/%d')
    marca=models.CharField(max_length=100)
    modelo=models.CharField(max_length=200)
    observacion=models.TextField()
    id_categoria=models.ForeignKey(Categoria)

    def _get_precioVentaFinal(self):
        if self.descuento == 0:
            return self.precio_venta
        else:
            return self.precio_venta - ((self.precio_venta * self.descuento)/100)
    precioFinal=property(_get_precioVentaFinal)

    def _get_descuento_mensaje(self):
        if self.descuento == 0:
            return ""
        else:
            return "OFERTA"
    ofertaMensaje=property(_get_descuento_mensaje)

    def __unicode__(self):
        return self.nombre

class Detalle_Factura(models.Model):
    id_producto=models.ForeignKey(Producto)
    id_factura=models.ForeignKey(Factura)
    cantidad_produco=models.PositiveIntegerField()
    precio_unitario=models.DecimalField(max_digits=8, decimal_places=2)
    precio_total=models.DecimalField(max_digits=8, decimal_places=2)

class Compra_Online(models.Model):
    pais=models.CharField(max_length=100)
    provincia=models.CharField(max_length=100)
    ciudad=models.CharField(max_length=100)
    postal=models.CharField(max_length=100)
    id_cliente=models.ForeignKey(Cliente)
    id_factura=models.ForeignKey(Factura)
    estado=models.CharField(max_length=50)#entregado #espera #enviado
    obervacion=models.TextField(blank=True, null=True)


class emp_trasporte(models.Model):
    trans_id = models.AutoField(primary_key=True)
    trans_nombre = models.CharField(max_length=50)
    trans_costo  = models.DecimalField(max_digits=10,decimal_places=2)
    trans_url = models.CharField(max_length=50)

    def __unicode__(self):
        return self.trans_nombre