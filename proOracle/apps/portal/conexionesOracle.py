from apps.portal.models import *
from apps.portal.views import *


def productosUltimosQuery():
    ultimosProductos=Producto.objects.exclude(descuento=0)[:3]
    return ultimosProductos

def categoriasQuery():
    categorias=Categoria.objects.all()
    return categorias

def categoriasQueryById(pk):
    nombreCategoria = Categoria.objects.get(id=pk).nombre
    return nombreCategoria

def productosByCategoria(pk):
    productos=Producto.objects.filter(id_categoria_id=pk)
    return productos

def productosAll():
    productos=Producto.objects.all()
    return productos

def productosById(pk):
    producto = Producto.objects.get(id=pk)
    return producto

def categoriasByFilter(pk):
    categorias=Categoria.objects.filter(id=pk)
    return categorias

def categoriasQueryByIdAll(pk):
    categoria = Categoria.objects.get(id=pk)
    return categoria

def guardarProductoById(id_pro, codigo, nombre, dcorta, stock, precioc, preciov, iva, descuento, modelo, categoria, imagen, boolean1):
    if(boolean1==0):
        guardar=Producto.objects.get(id=id_pro)
        guardar.codigo=codigo
        guardar.nombre=nombre
        guardar.descripcion_corta=dcorta
        guardar.descripcion_larga=''
        guardar.stock=stock
        guardar.precio_compra=precioc
        guardar.precio_venta=preciov
        iva1=False
        if(iva=='si'):
            iva1=True
        guardar.iva=iva1
        guardar.descuento=descuento
        guardar.imagen=imagen
        guardar.isActive=True
        guardar.marca=''
        guardar.modelo=modelo
        guardar.observacion=''
        guardar.id_categoria_id=int(categoria)
        guardar.save()
        dic = {
            'mensaje': "Se actualizo correctamente"
            }
    else:
        print('es sin')
        try:
            guardar=Producto.objects.get(id=id_pro)
            guardar.codigo=codigo
            guardar.nombre=nombre
            guardar.descripcion_corta=dcorta
            guardar.descripcion_larga=''
            guardar.stock=stock
            guardar.precio_compra=precioc
            guardar.precio_venta=preciov
            iva1="No"
            if(iva=='si'):
                iva1="Si"
            guardar.iva=iva1
            guardar.descuento=descuento
            guardar.marca=''
            guardar.modelo=modelo
            guardar.observacion=''
            guardar.id_categoria_id=int(categoria)
            guardar.save()
            dic = {
                'mensaje': "Se actualizo correctamente"
            }
        except Exception as e:
            dic = {
            'mensaje': "1"
            }
    return dic


def productosByFilter(codigo):
    productos=Producto.objects.filter(codigo = codigo)
    return productos

def guardarProductoByCodigo(codigo, nombre, dcorta, stock, precioc, preciov, iva, descuento, modelo, categoria):
    try:
        guardar=Producto.objects.get(codigo=codigo)
        guardar.codigo=codigo
        guardar.nombre=nombre
        guardar.descripcion_corta=dcorta
        guardar.descripcion_larga=''
        guardar.stock=stock
        guardar.precio_compra=precioc
        guardar.precio_venta=preciov
        iva1="No"
        if(iva=='si'):
            iva1="Si"
        guardar.iva=iva1
        guardar.descuento=descuento
        guardar.marca=''
        guardar.modelo=modelo
        guardar.observacion=''
        guardar.id_categoria_id=int(categoria)
        guardar.save()
        dic = {
            'mensaje': "Se actualizo correctamente"
        }
    except Exception as e:
        dic = {
        'mensaje': "1"
            }
    return dic

def guardarProductoQuery(codigo, nombre, dcorta, stock, precioc, preciov, iva, descuento, modelo, categoria, imagen):
    try:
        guardar=Producto(codigo=codigo, nombre=nombre, descripcion_corta=dcorta,descripcion_larga='',stock=stock,precio_compra=precioc,precio_venta=preciov, iva=iva, descuento=descuento, is_active=True,
                                 imagen=imagen, marca='', modelo=modelo, observacion='', id_categoria_id=int(categoria[0]))
        guardar.save()
        dic = {
            'mensaje': "Se guardo correctamente"
        }
    except Exception as e:
        print(e)
        dic = {
        'mensaje': "1"
            }
    return dic


def guardarCategoriaQuery(nombre, descripcion):
    guardar=Categoria(nombre=nombre,descripcion=descripcion)
    guardar.save()

def modificarCategoriaById(id_pro, nombre, dcorta):
    guardar=Categoria.objects.get(id=id_pro)
    guardar.nombre=nombre
    guardar.descripcion=dcorta
    guardar.save()


#CLIENTES
def clientesAll():
    clts=Cliente.objects.all()
    return clts



def guardarClienteQuery(ruc, nombre, apellido, email):
    try:
        guardar=Cliente(ruc_cedula=ruc, nombre=nombre, apellido=apellido, correo=emails)
        guardar.save()
        dic = {
            'mensaje': "Se guardo correctamente"
        }
    except Exception as e:
        print(e)
        dic = {
        'mensaje': "1"
            }
    return dic

def ClientesById(pk):
    clt = Cliente.objects.get(id=pk)
    return clt


def guardarClienteById(id_cli, ruc, nombre, apellido, email):
    guardar=Cliente.objects.get(id=id_cli)
    guardar.ruc_cedula=ruc
    guardar.nombre=nombre
    guardar.apellido=apellido
    guardar.correo=email
    guardar.save()
    dic = {
        'mensaje': "Se actualizo correctamente"
    }
    return dic