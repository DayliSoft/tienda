# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url, include
from .views import *

urlpatterns = patterns('',
    url(r'^$', index, name="URLindex"),
    url(r'^categorias/', categorias, name="URLcategorias"),
    url(r'^cat_guardar/$', guardarCategoria, name="URLguardarCategoria"),
    url(r'^cat_modificar/', modificarCategoria, name="URLmodificar"),
    url(r'^cat_modificar_guardar/', modificarCategoriaGuardar, name="URLmodificarguardar"),
    url(r'^categoria/(?P<parametro>\d+)/$', categoriaBusq , name="URLcategoria"),
    #PRODUCTOS
    url(r'^productos/', productos, name="URLproductos"),
    url(r'^pro_activar/', activarProducto, name="URLremover"),
    url(r'^pro_modificar/', modificarProducto, name="URLmodificar"),
    url(r'^pro_modificar_guardar/', modificarProductoGuardar, name="URLmodificarguardar"),
    url(r'^pro_guardar/$', guardarProducto, name="URLguardarProducto"),
    #CLIENTES
    url(r'^clientes/', clientes, name="URLclientes"),
    url(r'^cli_eliminar/', eliminarCliente, name="URLremoverCli"),
    url(r'^cli_modificar/', modificarCliente, name="URLmodificarCliente"),
    url(r'^cli_modificar_guardar/', modificarClienteGuardar, name="URLmodificarguardar"),
    url(r'^cli_guardar/$', guardarCliente, name="URLguardarCliente"),


)
