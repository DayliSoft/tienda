from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView
from django.conf.urls.static import static
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',
        url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('apps.portal.urls', namespace="URLportal")),
    url(r'^users/', include('apps.administracion.urls', namespace="URLAdministracion")),
    url(r'^compras/', include('apps.compras.urls', namespace="URLCompras")),
    (r'^$',     RedirectView.as_view(url='/')

),
) +  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)