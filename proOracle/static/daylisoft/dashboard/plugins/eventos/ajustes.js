/*Impuestos crud*/
function ajus_agregar_impuesto(url){
    if($("#formulario_ajustes_impuesto").valid() == true) {
        var form = document.forms.namedItem("formulario_ajustes_impuesto");
        datos = new FormData(form);
        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');

                    var dTable = $('#tabla_impuesto').DataTable();
                    var ii = "('/dashboard/impuesto/cargar', "+json.id+", this)";
                    var ij = "('/dashboard/impuesto/eliminar?id="+json.id+"','"+$('#ajustes_impuesto_nombre').val()+"', this)";
                    botones = '<div class="btn-group"><a href="#" onclick="ajus_cargar_impuesto'+ii+'" class="btn bg-navy btn-xs" title="Editar"><i class="glyphicon glyphicon-edit"></i></a>' +
                        '<a href="#" onclick="ajus_eliminar_impuesto'+ij+'" class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash-o"</i></a></div>';

                    //SI ES EDITAR: PREGUNTAR SI HAY ID Y BORRAR ESA FILA
                    if($("#ajustes_id_impuesto").val() != ""){
                        oculto_this = $("body").data("test").boton_this;
                        dTable.row($(oculto_this).closest('tr')).remove().draw(false);
                    }
                    dTable.row.add([
                        $('#ajustes_impuesto_nombre').val(),
                        $('#ajustes_impuesto_porcentaje').val()+" %",
                        botones
                    ]).draw(false);

                    reset_formulario('formulario_ajustes_impuesto', 'ajustes_impuesto_nombre');
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }                
            },
            error: function(e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function ajus_cargar_impuesto(url, id, boton){
    crear_alerta_gif();

    $.ajax({
        type: "GET",
        url:url+"?id="+id,
        dataType: "json",
        async: true,
        processData: false,
        contentType: false,
        success: function(json){
            swal.close();
            if (json.result == "OK"){
                $("#ajustes_id_impuesto").val(json.id);
                $("#ajustes_impuesto_nombre").val(json.nombre);
                $("#ajustes_impuesto_porcentaje").val(json.porcentaje);
                //Almacenar temporalmente el this (boton)
                $("body").data("test", {boton_this: boton });
            }else{
                toastr.options={"progressBar": true ,"showDuration": "500", "hideDuration": "3000", "timeOut": "8000","extendedTimeOut": "3000"};
                toastr.error(json.message,'Estado');
            }
            $("#ajustes_impuesto_nombre").focus();
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}

/*Empresa crud*/
function ajus_actualizar_empresa(url){
    if($("#formulario_ajustes_empresa").valid() == true) {
        var form = document.forms.namedItem("formulario_ajustes_empresa");
        datos = new FormData(form);
        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.info(json.message, 'Correcto');
                    if(json.bandera != 0){$("#btn_perfil_restablecer_logo").remove();}
                }else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function(e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

/*Transporte crud*/
function ajus_cargar_transporte(url, boton){
    crear_alerta_gif();

    $.ajax({
        type: "GET",
        url:url,
        dataType: "json",
        async: true,
        processData: false,
        contentType: false,
        success: function(json){
            swal.close();
            if (json.result == "OK"){
                $("#ajustes_transporte_nombre").val(json.nombre);
                $("#ajustes_transporte_costo").val(json.costo.replace(",", "."));
                $("#ajustes_transporte_url").val(json.url);
                $("#menu_ajustes_transporte").data("test", {transporte_id: json.id, boton_this: boton});
            }else{
                toastr.options={"progressBar": true ,"showDuration": "500", "hideDuration": "3000", "timeOut": "8000","extendedTimeOut": "3000"};
                toastr.error(json.message,'Estado');
            }
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}

function ajus_agregar_transporte(url){
    if($("#formulario_ajustes_transporte").valid() == true) {
        var form = document.forms.namedItem("formulario_ajustes_transporte");
        datos = new FormData(form);
        datos.append('transporte_id', $("#menu_ajustes_transporte").data("test").transporte_id);
        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');

                    var dTable = $('#tabla_transporte').DataTable();
                    var ii = "('/dashboard/transporte/cargar?id="+json.id+"', this)";
                    var ij = "('/dashboard/transporte/eliminar?id="+json.id+"','TT', 'tabla_transporte', this)";
                    botones = '<div class="btn-group"><button onclick="ajus_cargar_transporte'+ii+'" class="btn bg-navy btn-xs" title="Editar"><i class="glyphicon glyphicon-edit"></i></button>' +
                        '<button onclick="prod_eliminar_objetos'+ij+'" class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash-o"</i></button></div>';

                    //SI ES EDITAR: PREGUNTAR SI HAY ID Y BORRAR ESA FILA
                    if($("#menu_ajustes_transporte").data("test").transporte_id != ""){
                        oculto_this = $("#menu_ajustes_transporte").data("test").boton_this;
                        dTable.row($(oculto_this).closest('tr')).remove().draw(false);
                    }
                    dTable.row.add([
                        json.nombre,
                        "$ "+json.costo.replace(".", ","),
                        json.url,
                        botones
                    ]).draw(false);

                    ajus_cancelar_accion();
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }                
            },
            error: function(e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function ajus_cancelar_accion() {
    reset_formulario('formulario_ajustes_transporte','ajustes_transporte_nombre');
    $("#menu_ajustes_transporte").data("test", {transporte_id: ""});
}

/*Categorias crud*/
function ajus_cargar_categoria(url, boton){
    crear_alerta_gif();

    $.ajax({
        type: "GET",
        url:url,
        dataType: "json",
        async: true,
        processData: false,
        contentType: false,
        success: function(json){
            swal.close();
            if (json.result == "OK"){
                if(json.categoria != ""){                    
                    $("#id_categoria_padre").val(json.padre);
                    $("#id_categoria_padre").select2({
                        selected: json.padre
                    });
                    if(json.padre == null){ $('#tipo_entrada').bootstrapToggle('on'); }
                    else{ $('#tipo_entrada').bootstrapToggle('off'); }
                }            
                
                $("#id_categoria_hijo").val(json.nombre);
                $("#categoria_descripcion").val(json.descripcion);
                $("#menu_ajustes_categorias").data("test", {categoria_id: json.id, boton_this: boton});
            }else{
                toastr.options={"progressBar": true ,"showDuration": "500", "hideDuration": "3000", "timeOut": "8000","extendedTimeOut": "3000"};
                toastr.error(json.message,'Estado');
            }
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}

function ajus_agregar_categoria(url){
    if($("#formulario_producto_categoria").valid() == true) {
        var form = document.forms.namedItem("formulario_producto_categoria");
        datos = new FormData(form);
        datos.append('categoria_id', $("#menu_ajustes_categorias").data("test").categoria_id);
        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');

                    var dTable = $('#tabla_categorias').DataTable();
                    var ii = "('/dashboard/productos/categorias/cargar?id="+json.id+"', this)";
                    var ij = "('/dashboard/productos/categorias/eliminar?id="+json.id+"','CC', 'tabla_categorias', this)";
                    botones = '<div class="btn-group"><button onclick="ajus_cargar_categoria'+ii+'" class="btn bg-navy btn-xs" title="Editar"><i class="glyphicon glyphicon-edit"></i></button>' +
                        '<button onclick="prod_eliminar_objetos'+ij+'" class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash-o"</i></button></div>';

                    //SI ES EDITAR: PREGUNTAR SI HAY ID Y BORRAR ESA FILA
                    if($("#menu_ajustes_categorias").data("test").categoria_id != ""){
                        oculto_this = $("#menu_ajustes_categorias").data("test").boton_this;
                        dTable.row($(oculto_this).closest('tr')).remove().draw(false);
                    }
                    nombre = json.nombre;
                    if(Number(json.padre) > 0){
                        nombre += ", <span class='label label-waring'>"+json.padre_nombre+"</span>"
                    }                    
                    dTable.row.add([
                        json.id,
                        nombre,
                        botones
                    ]).draw(false);

                    ajus_cancelar_categoria();
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function(e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function ajus_cancelar_categoria() {
    reset_formulario('formulario_producto_categoria','id_categoria_hijo');
    $("#id_categoria_padre").val("0");
    $("#id_categoria_padre").select2({ selected: "0" });
    $("#menu_ajustes_categorias").data("test", {categoria_id: ""});
}

/*Marcas crud*/
function ajus_cargar_marcas(url, boton){
    crear_alerta_gif();

    $.ajax({
        type: "GET",
        url:url,
        dataType: "json",
        async: true,
        processData: false,
        contentType: false,
        success: function(json){
            swal.close();
            if (json.result == "OK"){
                $("#marca_producto").val(json.nombre);
                $("#menu_ajustes_marcas_administrar").data("test", {marca_id: json.id, boton_this: boton});
            }else{
                toastr.options={"progressBar": true ,"showDuration": "500", "hideDuration": "3000", "timeOut": "8000","extendedTimeOut": "3000"};
                toastr.error(json.message,'Estado');
            }
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}

function ajus_agregar_marcas(url){
    if($("#formulario_producto_marca").valid() == true) {
        var form = document.forms.namedItem("formulario_producto_marca");
        datos = new FormData(form);
        datos.append('marca_id', $("#menu_ajustes_marcas_administrar").data("test").marca_id);
        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');

                    var dTable = $('#tabla_marcas').DataTable();
                    var ii = "('/dashboard/productos/marcas/cargar?id="+json.id+"', this)";
                    var ij = "('/dashboard/productos/marcas/eliminar?id="+json.id+"','CC', 'tabla_marcas', this)";
                    botones = '<div class="btn-group"><button onclick="ajus_cargar_marcas'+ii+'" class="btn bg-navy btn-xs" title="Editar"><i class="glyphicon glyphicon-edit"></i></button>' +
                        '<button onclick="prod_eliminar_objetos'+ij+'" class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash-o"</i></button></div>';

                    //SI ES EDITAR: PREGUNTAR SI HAY ID Y BORRAR ESA FILA
                    if($("#menu_ajustes_marcas_administrar").data("test").marca_id != ""){
                        oculto_this = $("#menu_ajustes_marcas_administrar").data("test").boton_this;
                        dTable.row($(oculto_this).closest('tr')).remove().draw(false);
                    }                    
                    dTable.row.add([
                        json.id,
                        json.nombre,
                        botones
                    ]).draw(false);

                    ajus_cancelar_marca();
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function(e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function ajus_cancelar_marca() {
    reset_formulario('formulario_producto_marca','marca_producto');
    $("#menu_ajustes_marcas_administrar").data("test", {marca_id: ""});
}