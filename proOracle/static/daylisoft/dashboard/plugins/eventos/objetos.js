function cli_agregar_cliente(url, id){
    if($("#formulario_agregar_cliente").valid() == true) {
        var form = document.forms.namedItem("formulario_agregar_cliente");
        datos = new FormData(form);
        datos.append('id_cliente', id);

        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');
                    if (json.bandera == "E") {
                        $("#btn_restablecer_avatar").remove();
                    }
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function (e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function cli_cambiar_superusuario(url, objeto) {
    crear_alerta_gif();
    $.ajax({
        type: "GET",
        url: url+"&&estado="+$(objeto).prop('checked'),
        dataType: 'json',

        success: function(json){
            if (json.result == "OK"){
                swal.close();
            }else{
                swal("Ha ocurrio un error", json.message, "error");
            }
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}

function cli_activar_usuario(url, objeto) {
    crear_alerta_gif();
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',

        success: function(json){
            if (json.result == "OK"){
                swal.close();
                var eli = "('/dashboard/clientes/eliminar?id="+json.id+"', 'CL', 'tabla_clientes', this)";
                $(objeto).closest('tr').find(".btn-group").append('<button class="btn btn-danger btn-xs" onclick="prod_eliminar_objetos'+eli+'" title="Eliminar"><i class="fa fa-trash-o"></i></button>');
                $(objeto).closest('tr').find(".label-danger").text("Activo").removeClass('label-danger').addClass('label-primary');
                $(objeto).remove();
            }else{
                swal("Ha ocurrio un error", json.message, "error");
            }
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}

//Proveedores
function prov_agregar_proveedor(url, id){
    if($("#formulario_agregar_proveedor").valid() == true) {
        var form = document.forms.namedItem("formulario_agregar_proveedor");
        datos = new FormData(form);
        datos.append('id_proveedor', id);

        crear_alerta_gif();

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                swal.close();
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function (e) {
                swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function prov_activar_proveedor(url, objeto) {
    crear_alerta_gif();
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',

        success: function(json){
            if (json.result == "OK"){
                swal.close();
                var eli = "('/dashboard/proveedores/eliminar?id="+json.id+"', 'PV', 'tabla_proveedores', this)";
                $(objeto).closest('tr').find(".btn-group").append('<button class="btn btn-danger btn-xs" onclick="prod_eliminar_objetos'+eli+'" title="Eliminar"><i class="fa fa-trash-o"></i></button>');                
                $(objeto).remove();
            }else{
                swal("Ha ocurrio un error", json.message, "error");
            }
        },
        error: function(e) {
            swal("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", "error");
            console.log(e);
        }
    });
}